﻿#include "rotations.h"
#include "matrix.h"
#include <cmath>
#include <limits>

/*************************************************************************
Применение последовательности элементарных вращений к матрице

Алгоритм  умножает  матрицу слева   на  последовательность  преобразований
вращения, заданную массивами C и S. В зависимости от  параметра  IsForward
вращению подвергаются последовательно либо 1 и 2, 3 и 4 и т.д. строки (если
IsForward=True), либо N и N-1-ая, N-2 и N-3 и т.д. строки.

Преобразованию подвергается не вся матрица а только её часть (строки от M1
до M2, столбцы от N1 до N2). Элементы, не попавшие в указанную подматрицу,
остаются без изменений.

Входные параметры:
    IsForward   -   последовательность применения вращений
    M1,M2       -   преобразованию подвергаются строки от M1 до M2
    N1,N2       -   преобразованию подвергаются столбцы от N1 до N2
    C,S         -   коэффициенты преобразования.
                    Массив с нумерацией элементов [1..M2-M1]
    A           -   обрабатываемая матрица
    WORK        -   рабочий массив с нумерацией элементов [N1..N2]

Выходные параметры
    A           -   преобразованная матрица

Служебная подпрограмма
*************************************************************************/
void applyrotationsfromtheleft(bool isforward,
                               int m1, int m2,
                               int n1, int n2,
                               const std::vector<double>& c,
                               const std::vector<double>& s,
                               matrix::Matrix& a,
                               std::vector<double>& work)
{
  int j;
  int jp1;
  double ctemp;
  double stemp;
  double temp;

  if( m1>=m2||n1>=n2 )
    return;

  // Form  P * A
  if( isforward ) {
    if(n2-n1>1) {
      // Common case: N1<>N2
      for(j = m1; j < m2-1; j++) {
        ctemp = c[j-m1];
        stemp = s[j-m1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          jp1 = j+1;
          for(size_t k=n1; k<n2; ++k)
            work[k] = a.elem(jp1,k)*ctemp - a.elem(j, k)*stemp;
          for(size_t k=n1; k<n2; ++k)
            a.elem(j,k) = a.elem(j,k)*ctemp + a.elem(jp1,k)*stemp;
          for(size_t k=n1; k<n2; ++k)
            a.elem(jp1,k) = work[k];
//          ap::vmove(&work(n1), &a(jp1, n1), ap::vlen(n1,n2), ctemp);
//          ap::vsub(&work(n1), &a(j, n1), ap::vlen(n1,n2), stemp);
//          ap::vmul(&a(j, n1), ap::vlen(n1,n2), ctemp);
//          ap::vadd(&a(j, n1), &a(jp1, n1), ap::vlen(n1,n2), stemp);
//          ap::vmove(&a(jp1, n1), &work(n1), ap::vlen(n1,n2));
        }
      }
    }
    else {
      // Special case: N1=N2
      for(j = m1; j < m2-1; j++) {
        ctemp = c[j-m1];
        stemp = s[j-m1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          temp = a.elem(j+1,n1);
          work[n1] = a.elem(j+1, n1)*ctemp - a.elem(j, n1)*stemp;
          a.elem(j+1, n1) = ctemp*temp-stemp*a.elem(j, n1);
          a.elem(j, n1) = stemp*temp+ctemp*a.elem(j, n1);
        }
      }
    }
  }
  else {
    if(n2-n1>1) {
      // Common case: N1<>N2
      for(j = m2-2; j >= m1; j--) {
        ctemp = c[j-m1];
        stemp = s[j-m1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          jp1 = j+1;
          for(size_t k=n1; k<n2; ++k)
            work[k] = a.elem(jp1,k)*ctemp - a.elem(j, k)*stemp;
          for(size_t k=n1; k<n2; ++k)
            a.elem(j,k) = a.elem(j,k)*ctemp + a.elem(jp1,k)*stemp;
          for(size_t k=n1; k<n2; ++k)
            a.elem(jp1,k) = work[k];
//          ap::vmove(&work(n1), &a(jp1, n1), ap::vlen(n1,n2), ctemp);
//          ap::vsub(&work(n1), &a(j, n1), ap::vlen(n1,n2), stemp);
//          ap::vmul(&a(j, n1), ap::vlen(n1,n2), ctemp);
//          ap::vadd(&a(j, n1), &a(jp1, n1), ap::vlen(n1,n2), stemp);
//          ap::vmove(&a(jp1, n1), &work(n1), ap::vlen(n1,n2));
        }
      }
    }
    else {
      // Special case: N1=N2
      for(j = m2-2; j >= m1; j--) {
        ctemp = c[j-m1];
        stemp = s[j-m1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          temp = a.elem(j+1,n1);
          work[n1] = a.elem(j+1, n1)*ctemp - a.elem(j, n1)*stemp;
          a.elem(j+1,n1) = ctemp*temp-stemp*a.elem(j,n1);
          a.elem(j,n1) = stemp*temp+ctemp*a.elem(j,n1);
        }
      }
    }
  }
}


/*************************************************************************
Применение последовательности элементарных вращений к матрице

Алгоритм  умножает  матрицу справа   на  последовательность  преобразований
вращения, заданную массивами C и S. В зависимости от  параметра   IsForward
вращению подвергаются последовательно либо 1 и 2,  3  и  4  и т.д.  столбцы
(если IsForward=True), либо N-ый и N-1-ый, N-2-ой и N-3-ий и т.д. столбцы.

Преобразованию подвергается не вся матрица а только её часть (строки от M1
до M2, столбцы от N1 до N2). Элементы, не попавшие в указанную подматрицу,
остаются без изменений.

Входные параметры:
    IsForward   -   последовательность применения вращений
    M1,M2       -   преобразованию подвергаются строки от M1 до M2
    N1,N2       -   преобразованию подвергаются столбцы от N1 до N2
    C,S         -   коэффициенты преобразования.
                    Массив с нумерацией элементов [1..N2-N1]
    A           -   обрабатываемая матрица
    WORK        -   рабочий массив с нумерацией элементов [M1..M2]

Выходные параметры
    A           -   преобразованная матрица

Служебная подпрограмма
*************************************************************************/
void applyrotationsfromtheright(bool isforward,
                                int m1, int m2,
                                int n1, int n2,
                                const std::vector<double>& c,
                                const std::vector<double>& s,
                                matrix::Matrix& a,
                                std::vector<double>& work)
{
  int j;
  int jp1;
  double ctemp;
  double stemp;
  double temp;
  // Form A * P'
  if( isforward ) {
    if(m2-m1>1 ) {
      // Common case: M1<>M2
      for(j = n1; j < n2-1; j++) {
        ctemp = c[j-n1];
        stemp = s[j-n1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          jp1 = j+1;
          for(size_t k=m1; k<m2; ++k)
            work[k] = a.elem(k,jp1)*ctemp - a.elem(k,j)*stemp;
          for(size_t k=m1; k<m2; ++k)
            a.elem(k,j) = a.elem(k,j)*ctemp + a.elem(k,jp1)*stemp;
          for(size_t k=m1; k<m2; ++k)
            a.elem(k,jp1) = work[k];
//          ap::vmove(work.getvector(m1, m2), a.getcolumn(jp1, m1, m2), ctemp);
//          ap::vsub(work.getvector(m1, m2), a.getcolumn(j, m1, m2), stemp);
//          ap::vmul(a.getcolumn(j, m1, m2), ctemp);
//          ap::vadd(a.getcolumn(j, m1, m2), a.getcolumn(jp1, m1, m2), stemp);
//          ap::vmove(a.getcolumn(jp1, m1, m2), work.getvector(m1, m2));
        }
      }
    }
    else {
      // Special case: M1=M2
      for(j = n1; j < n2-1; j++) {
        ctemp = c[j-n1];
        stemp = s[j-n1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          temp = a.elem(m1,j+1);
          work[m1] = a.elem(m1,j+1)*ctemp - a.elem(m1,j)*stemp;
          a.elem(m1, j+1) = ctemp*temp-stemp*a.elem(m1,j);
          a.elem(m1, j) = stemp*temp+ctemp*a.elem(m1,j);
        }
      }
    }
  }
  else {
    if(m2-m1>1) {
      // Common case: M1<>M2
      for(j = n2-2; j >= n1; j--) {
        ctemp = c[j-n1];
        stemp = s[j-n1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          jp1 = j+1;
          for(size_t k=m1; k<m2; ++k)
            work[k] = a.elem(k,jp1)*ctemp - a.elem(k,j)*stemp;
          for(size_t k=m1; k<m2; ++k)
            a.elem(k,j) = a.elem(k,j)*ctemp + a.elem(k,jp1)*stemp;
          for(size_t k=m1; k<m2; ++k)
            a.elem(k,jp1) = work[k];
//          ap::vmove(work.getvector(m1, m2), a.getcolumn(jp1, m1, m2), ctemp);
//          ap::vsub(work.getvector(m1, m2), a.getcolumn(j, m1, m2), stemp);
//          ap::vmul(a.getcolumn(j, m1, m2), ctemp);
//          ap::vadd(a.getcolumn(j, m1, m2), a.getcolumn(jp1, m1, m2), stemp);
//          ap::vmove(a.getcolumn(jp1, m1, m2), work.getvector(m1, m2));
        }
      }
    }
    else {
      // Special case: M1=M2
      for(j = n2-2; j >= n1; j--) {
        ctemp = c[j-n1];
        stemp = s[j-n1];
        if(std::fabs(ctemp-1)>std::numeric_limits<double>::min() ||
           std::fabs(stemp)>std::numeric_limits<double>::min()) {
          temp = a.elem(m1,j+1);
          work[m1] = a.elem(m1,j+1)*ctemp - a.elem(m1,j)*stemp;
          a.elem(m1, j+1) = ctemp*temp-stemp*a.elem(m1,j);
          a.elem(m1, j) = stemp*temp+ctemp*a.elem(m1,j);
        }
      }
    }
  }
}


/*************************************************************************
Подпрограмма генерирует элементарное вращение, такое, что

[  CS  SN  ]  .  [ F ]  =  [ R ]
[ -SN  CS  ]     [ G ]     [ 0 ]

CS**2 + SN**2 = 1
*************************************************************************/
void generaterotation(double f, double g, double& cs, double& sn, double& r)
{
  if(std::fabs(g)<std::numeric_limits<double>::min()) {
    cs = 1;
    sn = 0;
    r = f;
    return;
  }
  if(std::fabs(f)<std::numeric_limits<double>::min()) {
    cs = 0;
    sn = 1;
    r = g;
    return;
  }
  r = std::sqrt(f*f+g*g);
  cs = f/r;
  sn = g/r;
  if(std::fabs(f)>std::fabs(g) && cs<0) {
    cs = -cs;
    sn = -sn;
    r = -r;
  }
}



