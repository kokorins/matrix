﻿#include "lq.h"
#include "reflections.h"
#include "matrix.h"
#include <algorithm>

namespace matrix {
/*************************************************************************
LQ-разложение прямоугольной матрицы размером M x N

Входные параметры:
    A   -   матрица A. Нумерация элементов: [0..M-1, 0..N-1]
    M   -   число строк в матрице A
    N   -   число столбцов в матрице A

Выходные параметры:
    A   -   матрицы L и Q в компактной форме (см. ниже)
    Tau -   массив скалярных множителей, участвующих в формировании
            матрицы Q. Нумерация элементов [0..Min(M,N)-1]

Матрица A представляется, как A = LQ, где Q ортогональная матрица размером
M x M, а R  нижнетреугольная  (или  нижнетрапецоидальная) матрица размером
M x N.

После завершения работы подпрограммы на главной диагонали матрицы A и ниже
располагаются элементы матрицы L. В массиве Tau и над  главной  диагональю
матрицы A располагаются элементы, формирующие матрицу Q, следующим способом:

Матрица Q представляется, как произведение элементарных отражений

Q = H(k-1)*H(k-2)*...*H(1)*H(0),

где k = min(m,n), а каждое H(i) имеет вид

H(i) = 1 - tau * v * (v^T)

где tau скалярная величина, хранящаяся в Tau[I], а v - вещественный вектор
у которого v(0:i-1)=0, v(i)=1, v(i+1:n-1) хранится в элементах A(i,i+1:n-1).
*************************************************************************/
void MatrixLq::Lq(Matrix& a, std::vector<double>& tau)
{
  size_t m = a.numRows();
  size_t n = a.numCols();
  size_t minmn = std::min(m, n);

  std::vector<double> work(m+1);
  std::vector<double> t(n+1);
  tau.resize(minmn);

  for(size_t i = 0; i < minmn; i++) {
    // Generate elementary reflector H(i) to annihilate A(i,i+1:n-1)
    for(size_t j=0; j<n-i; ++j)
      t[j] = a.elem(i,i+j);
    tau[i] = generatereflection(t, n-i);
    for(size_t j=0; j<n-i; ++j)
      a.elem(i,i+j) = t[j];
//    ap::vmove(&a(i, i), &t(1), ap::vlen(i,n-1));
    t[0] = 1;
    if( i<n-1) {
      // Apply H(i) to A(i+1:m,i:n) from the right
      applyreflectionfromtheright(a, tau[i], t, i+1, m, i, n, work);
    }
  }
}


/*************************************************************************
Частичная "распаковка" матрицы Q из LQ-разложения матрицы A.

Входные параметры:
    A       -   матрицы L и Q в упакованной форме.
                Результат работы RMatrixLQ.
    M       -   число строк в оригинальной матрице A. M>=0
    N       -   число столбцов в оригинальной матрице A. N>=0
    Tau     -   скалярные множители, формирующие Q.
                Результат работы RMatrixLQ.
    QRows   -   требуемое число строк матрицы Q. N>=QRows>=0.

Выходные параметры:
    Q       -   первые QRows строк матрицы Q. Массив с нумерацией
                элементов [0..QRows-1, 0..N-1]. Если QRows=0, то массив
                не изменяется.

*************************************************************************/
Matrix MatrixLq::UnpackQ(const Matrix& a, const std::vector<double>& tau,
                         size_t qrows)
{
  size_t m=a.numRows();
  size_t n=a.numCols();
  if(m<=0||n<=0||qrows<=0 )
    return a;
  // init
  size_t minmn = std::min(a.numRows(), a.numCols());
  size_t k = std::min(minmn, qrows);
  matrix::Matrix q(qrows, n);
  std::vector<double> v(n+1);
  std::vector<double> work(qrows);
  for(size_t i = 0; i < qrows; i++)
    for(size_t j = 0; j < n; j++)
      q.elem(i,j) = (i==j);
  // unpack Q
  for(int i = k-1; i >= 0; i--) {
    // Apply H(i)
    for(size_t j=0; j<n-i;++i)
      v[j]=a.elem(i,j+i);
//    ap::vmove(&v(1), &a(i, i), ap::vlen(1,n-i));
    v[0] = 1;
    applyreflectionfromtheright(q, tau[i], v, 0, qrows, i, n, work);
  }
  return q;
}

/*************************************************************************
Распаковка матрицы L из LQ-разложения матрицы A.

Входные параметры:
    A       -   матрицы Q и L в упакованной форме.
                Результат работы подпрограммы RMatrixLQ
    M       -   число строк в оригинальной матрице A. M>=0
    N       -   число столбцов в оригинальной матрице A. N>=0

Выходные параметры:
    L       -   матрица L. Массив с нумерацией элементов [0..M-1, 0..N-1].

*************************************************************************/
Matrix MatrixLq::UnpackL(const Matrix& a)
{
  size_t m = a.numRows();
  size_t n = a.numCols();
  if(m<=0||n<=0)
    return a;
  matrix::Matrix l(m, n);
  for(size_t i = 0; i < n; i++)
    l.elem(0,i) = 0;
  for(size_t i = 1; i < m; i++)
    for(size_t j=0; j<n; ++j)
      l.elem(i,j)=l.elem(0,j);
//    ap::vmove(&l(i, 0), &l(0, 0), ap::vlen(0,n-1));
  for(size_t i = 0; i < m; i++) {
    size_t k = std::min(i, n);
    for(size_t j=0; j<k; ++j)
      l.elem(i,j) = a.elem(i,j);
  }
  return l;
}

MatrixLq::MatrixLq(Matrix &a) : _a(a)
{
  Lq(_a, _tau);
}

const Matrix &MatrixLq::a() const
{
  return _a;
}

const std::vector<double> &MatrixLq::tau() const
{
  return _tau;
}

Matrix MatrixLq::unpackQ(size_t num_rows) const
{
  return UnpackQ(_a, _tau, num_rows);
}

Matrix MatrixLq::unpackL() const
{
  return UnpackL(_a);
}

} //namespace matrix

