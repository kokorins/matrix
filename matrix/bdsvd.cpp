﻿#include "bdsvd.h"
#include "rotations.h"
#include "matrix.h"
#include <limits>
#include <cmath>
#include <iostream>
#include <iterator>
#include <cstdlib>
#include <algorithm>

double extsignbdsqr(double a, double b);
void svd2x2(double f, double g, double h, double& ssmin, double& ssmax);
void svdv2x2(double f, double g, double h, double& ssmin, double& ssmax,
             double& snr, double& csr, double& snl, double& csl);

std::vector<double> CalcSingVec(matrix::Matrix& m, int mm0, int mm1, bool byrow, double sinr, double cosr)
{
  int n = m.numCols();
  if(byrow)
    n = m.numRows();
  std::vector<double> res(n);
  for(size_t k=0; k<n; ++k) {
    if(byrow)
      res[k] = m.elem(k,mm1)*cosr + m.elem(k,mm0)*sinr;
    else
      res[k] = m.elem(mm1,k)*cosr + m.elem(mm0,k)*sinr;
  }
  for(size_t k=0; k<n; ++k) {
    if(byrow)
      m.elem(k,mm0) = m.elem(k, mm0)* cosr - m.elem(k,mm1)*sinr;
    else
      m.elem(mm0,k) = m.elem(mm0,k)* cosr - m.elem(mm1, k)*sinr;
  }
  for(size_t k=0; k<n; ++k) {
    if(byrow)
      m.elem(k,mm1) = res[k];
    else
      m.elem(mm1,k) = res[k];
  }
  return res;
}

/*************************************************************************
Сингулярное разложение двухдиагональной матрицы (расширенный алгоритм)

Алгоритм  осуществляет  сингулярное  разложение двухдиагональной матрицы B
(верхней или нижней), представляя её в виде B = Q * S * P^T, где Q и  P  -
ортогональные матрицы, S - диагональная с неотрицательными  элементами  на
главной диагонали, расположенными в порядке убывания.

Алгоритм находит сингулярные значения. Дополнительно алгоритм может вычислять
матрицы Q и P (точнее, не сами матрицы, а их произведения на заданные матрицы
U и VT - U*Q и (P^T)*VT)). Разумеется, матрицы U и VT  могут  быть  какими
угодно,  в  том  числе  и  единичными.  Также  алгоритм  может   вычислять
произведение Q'*C (это произведение вычисляется более эффективно, чем U*Q,
потому что при этом проводятся операции со строками,  а  не  со  столбцами
матрицы).

Особенностью алгоритма  является   возможность  находить  ВСЕ  сингулярные
значения,  в  том  числе  и  сколь  угодно близкие к нолю, с относительной
точностью, близкой к машинной. Если параметр  IsFractionalAccuracyRequired
равен True, то все сингулярные значения находятся с высокой  относительной
точностью, близкой к машинной. Если параметр равен False, то с  точностью,
близкой к машинной, находятся лишь самое большое сингулярное значение,   а
абсолютная погрешность остальных  сингулярных  значений  равна  абсолютной
погрешности самого большого сингулярного значения.

Входные параметры:
    D   -       главная диагональ матрицы B.
                Массив с нумерацией элементов [0..N-1]
    E   -       наддиагональ (или поддиагональ) матрицы B.
                Массив с нумерацией элементов [0..N-2]
    N   -       размер матрицы B
    IsUpper-    True, если матрица верхняя двухдиагональная.
    IsFractionalAccuracyRequired-
                с какой точностью надо искать сингулярные значения
    U   -       матрица, которая будет  умножена на матрицу  Q.  Массив  с
                нумерацией элементов [0..NRU-1, 0..N-1].  Матрица  может  быть
                больше, в таком случае  лишь  указанная  подматрица  будет
                умножена на матрицу Q.
    NRU -       число строк в матрице U
    C   -       матрица, которая будет  умножена на матрицу Q'.  Массив  с
                нумерацией элементов [0..N-1, 0..NCC-1].  Матрица  может  быть
                больше, в таком случае  лишь  указанная  подматрица  будет
                умножена на матрицу Q'.
    NCC -       число столбцов в матрице C
    VT  -       матрица, которая будет  умножена на матрицу  P^T. Массив с
                нумерацией элементов [0..N-1, 0..NCVT-1]. Матрица  может  быть
                больше, в таком случае  лишь  указанная  подматрица  будет
                умножена на матрицу P^T.
    NCVT-       число столбцов в матрице VT.

Выходные параметры:
    D   -       сингулярные значения матрицы B, упорядоченные по убыванию.
    U   -       если NRU>0,  содержит матрицу U*Q
    VT  -       если NCVT>0, содержит матрицу (P^T)*VT
    C   -       если NCC>0,  содержит матрицу Q'*C

Результат:
    True, если алгоритм сошелся
    False, если алгоритм не сошелся (редчайший случай)

Дополнительная информация:
    тип сходимости контролируется внутренним параметром TOL. Если параметр
    больше   ноля,  то  сингулярные  значения  находятся  с  относительной
    погрешностью TOL. Если TOL<0, сингулярные значения находятся с
    абсолютной погрешностью ABS(TOL)*norm(B).
    По умолчанию |TOL| находится в интервале от 10*Epsilon до 100*Epsilon,
    где Epsilon - машинная точность. Не рекомендуется делать  TOL  меньше,
    чем 10*Epsilon, поскольку это сушественно  замедлит  алгоритм,  но  не
    обязательно приведет к дальнейшему уменьшению погрешности.

  -- LAPACK routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     October 31, 1999.
*************************************************************************/
bool matrixbdsvd(std::vector<double>& d,
                 std::vector<double> e,
                 bool isupper,
                 bool isfractionalaccuracyrequired,
                 matrix::Matrix& u,
                 matrix::Matrix& c,
                 matrix::Matrix& vt)
{
  int nru = u.numRows();
  int ncc = c.numCols();
  int ncvt = vt.numCols();
  size_t n = d.size();
  if(n==0)
    return true;
  if(n==1) {
    if(d[0]<0) {
      d[0] = -d[0];
      if(ncvt>0)
        for(size_t i=0; i<ncvt; ++i) // ap::vmul(&vt(0, 0), ap::vlen(0,0+ncvt-1), -1);
          vt.elem(0, i)*=-1;
    }
    return true;
  }
  int i;
  int isub;
  int iter;
  int j;
  int ll;
  int lll;
  int m;
  int maxit;
  int oldll;
  int oldm;
  double abse;
  double abss;
  double cosl;
  double cosr;
  double cs;
  double f;
  double g;
  double h;
  double mu;
  double oldcs;
  double oldsn;
  double r;
  double shift;
  double sinl;
  double sinr;
  double sll;
  double smax;
  double smin;
  double sminl;
  double sminlo;
  double sminoa;
  double sn;
  double thresh;
  double tol;
  double tolmul;
  std::vector<double> work0(n-1);
  std::vector<double> work1(n-1);
  std::vector<double> work2(n-1);
  std::vector<double> work3(n-1);
  double tmp;
  bool bchangedir;
// init
  std::vector<double> utemp(nru);
  std::vector<double> vttemp(ncvt);
  std::vector<double> ctemp(ncc);
  int maxitr = 12;
  bool fwddir = true;
  e.push_back(0);
  int idir = 0;
  // Get machine constants
  double eps = 5E-16;
  double unfl = std::numeric_limits<double>::min();
  // If matrix lower bidiagonal, rotate to be upper bidiagonal by applying rotations on the left
  if(!isupper) {
    for(i = 0; i < n-1; i++) {
      generaterotation(d[i], e[i], cs, sn, r);
      d[i] = r;
      e[i] = sn*d[i+1];
      d[i+1] = cs*d[i+1];
      work0[i] = cs;
      work1[i] = sn;
    }
    // Update singular vectors if desired
    if(nru>0)
      applyrotationsfromtheright(fwddir, 0, nru, 0, n, work0, work1, u, utemp);
    if(ncc>0)
      applyrotationsfromtheleft(fwddir, 0, n, 0, ncc, work0, work1, c, ctemp);
  }
  // Compute singular values to relative accuracy TOL
  // (By setting TOL to be negative, algorithm will compute
  // singular values to absolute accuracy ABS(TOL)*norm(input matrix))
  tolmul = std::max(10.0, std::min(100.0, pow(eps, -0.125)));
  tol = tolmul*eps;
  if(!isfractionalaccuracyrequired)
    tol = -tol;
  // Compute approximate maximum, minimum singular values
  smax = 0;
  for(i = 0; i < n; i++)
    smax = std::max(smax, std::fabs(d[i]));
  for(i = 0; i < n-1; i++)
    smax = std::max(smax, fabs(e[i]));
  sminl = 0;
  if(tol>=0) {
    // Relative accuracy desired
    sminoa = fabs(d[0]);
    if(sminoa!=0) {
      mu = sminoa;
      for(i = 1; i < n; i++) {
        mu = fabs(d[i])*(mu/(mu+fabs(e[i-1])));
        sminoa = std::min(sminoa, mu);
        if(sminoa==0)
          break;
      }
    }
    sminoa = sminoa/sqrt(double(n));
    thresh = std::max(tol*sminoa, maxitr*n*n*unfl);
  }
  else // Absolute accuracy desired
    thresh = std::max(fabs(tol)*smax, maxitr*n*n*unfl);
  // Prepare for main iteration loop for the singular values
  // (MAXIT is the maximum number of passes through the inner
  // loop permitted before nonconvergence signalled.)
  maxit = maxitr*n*n;
  iter = 0;
  oldll = -1;
  oldm = -1;
  m = n-1; // M points to last element of unconverged part of matrix
  while(true) { // Begin main iteration loop
    if(m<0) // Check for convergence or exceeding iteration count
      break;
    if(iter>maxit)
      return false;
    if(tol<0 && fabs(d[m])<=thresh) // Find diagonal block of matrix to work on
      d[m] = 0;
    smax = fabs(d[m]);
    smin = smax;
    bool matrixsplitflag = false;
    for(ll=m-1; ll>=0; --ll) {
      abss = fabs(d[ll]);
      abse = fabs(e[ll]);
      if(tol<0 && abss<=thresh)
        d[ll] = 0;
      if(abse<=thresh) {
        matrixsplitflag = true;
        break;
      }
      smin = std::min(smin, abss);
      smax = std::max(smax, std::max(abss, abse));
    }
    if(!matrixsplitflag)
      ll = -1;
    else {
      e[ll] = 0; // Matrix splits since E(LL) = 0
      if(ll==m-1) { // Convergence of bottom singular value, return to top of loop
        --m;
        continue;
      }
    }
    ++ll;
    // E(LL) through E(M-1) are nonzero, E(LL-1) is zero
    if(ll==m-1) {
      // 2 by 2 block, handle separately
      double sigmn, sigmx;
      svdv2x2(d[m-1], e[m-1], d[m], sigmn, sigmx, sinr, cosr, sinl, cosl);
      d[m-1] = sigmx;
      e[m-1] = 0;
      d[m] = sigmn;
      // Compute singular vectors, if desired
      if(ncvt>0)
        vttemp = CalcSingVec(vt, m, m-1, false, sinr, cosr);
      if(nru>0)
        utemp = CalcSingVec(u, m, m-1, true, sinl, cosl);
      if(ncc>0)
        ctemp = CalcSingVec(c, m, m-1, false, sinl, cosl);
      std::cout<<"vttemp: ";
      std::copy(vttemp.begin(), vttemp.end(),
                std::ostream_iterator<double>(std::cout, " "));
      std::cout<<std::endl;
      std::cout<<"utemp: ";
      std::copy(utemp.begin(), utemp.end(),
                std::ostream_iterator<double>(std::cout, " "));
      std::cout<<std::endl;
      std::cout<<"ctemp: ";
      std::copy(ctemp.begin(), ctemp.end(),
                std::ostream_iterator<double>(std::cout, " "));
      std::cout<<std::endl;
      m -= 2;
      continue;
    }
    // If working on new submatrix, choose shift direction
    // (from larger end diagonal element towards smaller)
    // Previously was
    //     "if (LL>OLDM) or (M<OLDLL) then"
    // fixed thanks to Michael Rolle < m@rolle.name >
    // Very strange that LAPACK still contains it.
    bchangedir = false;
    if(idir==1 && fabs(d[ll])<1.0E-3*fabs(d[m]))
      bchangedir = true;
    if(idir==2 && fabs(d[m])<1.0E-3*fabs(d[ll]))
      bchangedir = true;
    if(ll!=oldll || m!=oldm || bchangedir) {
      if(fabs(d[ll])>=fabs(d[m]))
        idir = 1;// Chase bulge from top (big end) to bottom (small end)
      else
        idir = 2;// Chase bulge from bottom (big end) to top (small end)
    }
    // Apply convergence tests
    if(idir==1) {
      // Run convergence test in forward direction
      // First apply standard test to bottom of matrix
      if(fabs(e[m-1])<=fabs(tol)*fabs(d[m]) || tol<0 && fabs(e[m-1])<=thresh) {
        e[m-1] = 0;
        continue;
      }
      if(tol>=0) {
        // If relative accuracy desired, apply convergence criterion forward
        mu = fabs(d[ll]);
        sminl = mu;
        bool iterflag = false;
        for(lll = ll; lll < m; lll++) {
          if(fabs(e[lll])<=tol*mu) {
            e[lll] = 0;
            iterflag = true;
            break;
          }
          sminlo = sminl;
          mu = fabs(d[lll+1])*(mu/(mu+fabs(e[lll])));
          sminl = std::min(sminl, mu);
        }
        if(iterflag)
          continue;
      }
    }
    else {
      // Run convergence test in backward direction. First apply standard test to top of matrix
      if(fabs(e[ll])<=fabs(tol)*fabs(d[ll]) || tol<0 && fabs(e[ll])<=thresh) {
        e[ll] = 0;
        continue;
      }
      if(tol>=0) {
        // If relative accuracy desired,
        // apply convergence criterion backward
        mu = fabs(d[m]);
        sminl = mu;
        bool iterflag = false;
        for(lll = m-1; lll >= ll; lll--) {
          if( fabs(e[lll])<=tol*mu ) {
            e[lll] = 0;
            iterflag = true;
            break;
          }
          sminlo = sminl;
          mu = fabs(d[lll])*(mu/(mu+fabs(e[lll])));
          sminl = std::min(sminl, mu);
        }
        if(iterflag)
          continue;
      }
    }
    oldll = ll;
    oldm = m;
    // Compute shift.  First, test if shifting would ruin relative
    // accuracy, and if so set the shift to zero.
    if(tol>=0 && n*tol*(sminl/smax) <= std::max(eps, 0.01*tol))
      shift = 0;// Use a zero shift to avoid loss of relative accuracy
    else {
      // Compute the shift from 2-by-2 block at end of matrix
      if(idir==1) {
        sll = fabs(d[ll]);
        svd2x2(d[m-1], e[m-1], d[m], shift, r);
      }
      else {
        sll = fabs(d[m]);
        svd2x2(d[ll], e[ll], d[ll+1], shift, r);
      }
      // Test if shift negligible, and if so set to zero
      if(sll>0) {
        if((shift*shift)/(sll*sll)<eps)
          shift = 0;
      }
    }
    // Increment iteration count
    iter += m-ll;
    std::cout<<"iter: "<<iter<<" m: "<<m<<std::endl;
    // If SHIFT = 0, do simplified QR iteration
    if(shift==0) {
      if(idir==1) {
        // Chase bulge from top to bottom
        // Save cosines and sines for later singular vector updates
        cs = 1;
        oldcs = 1;
        for(i = ll; i < m; i++) {
          generaterotation(d[i]*cs, e[i], cs, sn, r);
          if(i>ll)
            e[i-1] = oldsn*r;
          generaterotation(oldcs*r, d[i+1]*sn, oldcs, oldsn, tmp);
          d[i] = tmp;
          work0[i-ll] = cs;
          work1[i-ll] = sn;
          work2[i-ll] = oldcs;
          work3[i-ll] = oldsn;
        }
        h = d[m]*cs;
        d[m] = h*oldcs;
        e[m-1] = h*oldsn;
        // Update singular vectors
        if(ncvt>0) {
          applyrotationsfromtheleft(fwddir, ll, m+1, 0, ncvt, work0, work1, vt, vttemp);
        }
        if(nru>0) {
          applyrotationsfromtheright(fwddir, 0, nru, ll, m+1, work2, work3, u, utemp);
        }
        if(ncc>0) {
          applyrotationsfromtheleft(fwddir, ll, m+1, 0, ncc, work2, work3, c, ctemp);
        }
        // Test convergence
        if(fabs(e[m-1])<=thresh)
          e[m-1] = 0;
        std::cout<<"vttemp: ";
        std::copy(vttemp.begin(), vttemp.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;
        std::cout<<"utemp: ";
        std::copy(utemp.begin(), utemp.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;
        std::cout<<"ctemp: ";
        std::copy(ctemp.begin(), ctemp.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;

      }
      else {
          // Chase bulge from bottom to top
          // Save cosines and sines for later singular vector updates
          cs = 1;
          oldcs = 1;
          for(i = m; i > ll; i--) {
            generaterotation(d[i]*cs, e[i-1], cs, sn, r);
            if( i<m )
              e[i] = oldsn*r;
            generaterotation(oldcs*r, d[i-1]*sn, oldcs, oldsn, tmp);
            d[i] = tmp;
            work0[i-ll] = cs;
            work1[i-ll] = -sn;
            work2[i-ll] = oldcs;
            work3[i-ll] = -oldsn;
          }
          h = d[ll]*cs;
          d[ll] = h*oldcs;
          e[ll] = h*oldsn;
          // Update singular vectors
          if( ncvt>0 )
            applyrotationsfromtheleft(!fwddir, ll, m+1, 0, ncvt, work2, work3, vt, vttemp);
          if( nru>0 )
            applyrotationsfromtheright(!fwddir, 0, nru, ll, m+1, work0, work1, u, utemp);
          if( ncc>0 )
            applyrotationsfromtheleft(!fwddir, ll, m+1, 0, ncc, work0, work1, c, ctemp);
          // Test convergence
          if(fabs(e[ll])<=thresh)
            e[ll] = 0;
      }
    }
    else {
      // Use nonzero shift
      if( idir==1 ) {
        // Chase bulge from top to bottom
        // Save cosines and sines for later singular vector updates
        f = (fabs(d[ll])-shift)*(extsignbdsqr(1.0, d[ll])+shift/d[ll]);
        g = e[ll];
        std::cout<<f<<" "<<g<<"\n";
        for(i = ll; i < m; i++) {
          generaterotation(f, g, cosr, sinr, r);
          if( i>ll )
            e[i-1] = r;
          f = cosr*d[i]+sinr*e[i];
          e[i] = cosr*e[i]-sinr*d[i];
          g = sinr*d[i+1];
          d[i+1] *= cosr;
          generaterotation(f, g, cosl, sinl, r);
          d[i] = r;
          f = cosl*e[i]+sinl*d[i+1];
          d[i+1] = cosl*d[i+1]-sinl*e[i];
          if( i<m-1 ) {
            g = sinl*e[i+1];
            e[i+1] = cosl*e[i+1];
          }
          work0[i-ll] = cosr;
          work1[i-ll] = sinr;
          work2[i-ll] = cosl;
          work3[i-ll] = sinl;
        }
        e[m-1] = f;
        std::cout<<"work0: ";
        std::copy(work0.begin(), work0.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;
        std::cout<<"work1: ";
        std::copy(work1.begin(), work1.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;

        // Update singular vectors
        if( ncvt>0 ) {
          std::cout<<"vt:\n";
          std::cout<<vt;
          applyrotationsfromtheleft(fwddir, ll, m+1, 0, ncvt, work0, work1, vt, vttemp);
        }
        if( nru>0 )
          applyrotationsfromtheright(fwddir, 0, nru, ll, m+1, work2, work3, u, utemp);
        if( ncc>0 )
          applyrotationsfromtheleft(fwddir, ll, m+1, 0, ncc, work2, work3, c, ctemp);
        // Test convergence
        if(fabs(e[m-1])<=thresh)
          e[m-1] = 0;
        std::cout<<"vttemp: ";
        std::copy(vttemp.begin(), vttemp.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;
        std::cout<<"utemp: ";
        std::copy(utemp.begin(), utemp.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;
        std::cout<<"ctemp: ";
        std::copy(ctemp.begin(), ctemp.end(),
                  std::ostream_iterator<double>(std::cout, " "));
        std::cout<<std::endl;

      }
      else {
        // Chase bulge from bottom to top
        // Save cosines and sines for later singular vector updates
        f = (fabs(d[m])-shift)*(extsignbdsqr(1.0, d[m])+shift/d[m]);
        g = e[m-1];
        for(i = m; i > ll; i--) {
          generaterotation(f, g, cosr, sinr, r);
          if(i<m)
            e[i] = r;
          f = cosr*d[i]+sinr*e[i-1];
          e[i-1] = cosr*e[i-1]-sinr*d[i];
          g = sinr*d[i-1];
          d[i-1] = cosr*d[i-1];
          generaterotation(f, g, cosl, sinl, r);
          d[i] = r;
          f = cosl*e[i-1]+sinl*d[i-1];
          d[i-1] = cosl*d[i-1]-sinl*e[i-1];
          if( i>ll+1 ) {
            g = sinl*e[i-2];
            e[i-2] = cosl*e[i-2];
          }
          work0[i-ll-1] = cosr;
          work1[i-ll-1] = -sinr;
          work2[i-ll-1] = cosl;
          work3[i-ll-1] = -sinl;
        }
        e[ll] = f;
        // Test convergence
        if( fabs(e[ll])<=thresh )
          e[ll] = 0;
        // Update singular vectors if desired
        if( ncvt>0 )
          applyrotationsfromtheleft(!fwddir, ll, m+1, 0, ncvt, work2, work3, vt, vttemp);
        if( nru>0 )
          applyrotationsfromtheright(!fwddir, 0, nru, ll, m+1, work0, work1, u, utemp);
        if( ncc>0 )
          applyrotationsfromtheleft(!fwddir, ll, m+1, 0, ncc, work0, work1, c, ctemp);
      }
    }
    continue;// QR iteration finished, go back and check convergence
  }
  // All singular values converged, so make them positive
  for(i = 0; i < n; i++) {
    if(d[i]<0) {
      d[i] = -d[i];// Change sign of singular vectors, if desired
      if(ncvt>0) {
        for(size_t k=0; k<ncvt; ++k) // ap::vmul(&vt(i+0-1, 0), ap::vlen(0,ncvt), -1);
          vt.elem(i, k) *= -1;
      }
    }
  }
  // Sort the singular values into decreasing order (insertion sort on
  // singular values, but only one transposition per singular vector)
  for(i = 0; i < n-1; i++) {
    // Scan for smallest D(I)
    isub = 0;
    smin = d[0];
    for(j = 1; j < n-i; j++) {
      if(d[j]<=smin) {
        isub = j;
        smin = d[j];
      }
    }
    if(isub!=n-i-1) {
      // Swap singular values and vectors
      d[isub] = d[n-i-1];
      d[n-i-1] = smin;
      if(ncvt>0) {
        j = n-i-1;
        for(size_t k=0;k<ncvt; ++k)
          vttemp[k] = vt.elem(isub, k);
//        ap::vmove(&vttemp(0), &vt(isub+0-1, 0), ap::vlen(0,ncvt));
        for(size_t k=0;k<ncvt; ++k)
          vt.elem(isub, k) = vt.elem(j, k);
//        ap::vmove(&vt(isub+0-1, 0), &vt(j+0-1, 0), ap::vlen(0,ncvt));
        for(size_t k=0; k<ncvt; ++k)
          vt.elem(j, k) = vttemp[k];
//        ap::vmove(&vt(j+0-1, 0), &vttemp(0), ap::vlen(0,ncvt));
      }
      if( nru>0 ) {
        j = n-i-1;
        for(size_t k=0; k<ncvt; ++k)
          utemp[k] = vt.elem(isub, k);
//        ap::vmove(utemp.getvector(0, nru), u.getcolumn(isub+0-1, 0, nru));
        for(size_t k=0;k<nru; ++k)
          u.elem(k, isub) = u.elem(k, j);
//        ap::vmove(u.getcolumn(isub+0-1, 0, nru), u.getcolumn(j+0-1, 0, nru));
        for(size_t k=0;k<ncvt; ++k)
          u.elem(k, j) = utemp[k];
//        ap::vmove(u.getcolumn(j+0-1, 0, nru), utemp.getvector(0, nru));
      }
      if( ncc>0 ) {
        j = n-i-1;
        for(size_t k=0; k<ncc; ++k)
          ctemp[k] = c.elem(isub, k);
//        ap::vmove(&ctemp(0), &c(isub+0-1, 0), ap::vlen(0,ncc));
        for(size_t k=0; k<ncc; ++k)
          c.elem(isub, k) = c.elem(j, k);
//        ap::vmove(&c(isub+0-1, 0), &c(j+0-1, 0), ap::vlen(0,ncc));
        for(size_t k=0; k<ncc; ++k)
          c.elem(j, k) = ctemp[k];
//        ap::vmove(&c(j+0-1, 0), &ctemp(0), ap::vlen(0,ncc));
      }
    }
  }
  return true;
}


/*************************************************************************
Передача знака. Служебная подпрограмма.
*************************************************************************/
double extsignbdsqr(double a, double b)
{
  if( b>=0 )
    return fabs(a);
  else
    return -fabs(a);
}


/*************************************************************************
Svd2X2  computes the singular values of the 2-by-2 matrix
   [  F   G  ]
   [  0   H  ].
On return, SSMIN is the smaller singular value and SSMAX is the
larger singular value.

  -- LAPACK auxiliary routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     September 30, 1994
*************************************************************************/
void svd2x2(double f, double g, double h, double& ssmin, double& ssmax)
{
  double aas;
  double at;
  double au;
  double c;

  double fa = fabs(f);
  double ga = fabs(g);
  double ha = fabs(h);
  double fhmn = std::min(fa, ha);
  double fhmx = std::max(fa, ha);
  if(fhmn==0) {
    ssmin = 0;
    if(fhmx==0)
      ssmax = ga;
    else
      ssmax = std::max(fhmx, ga)*sqrt(1+std::min(fhmx, ga)*std::min(fhmx, ga)/(std::max(fhmx, ga)*std::max(fhmx, ga)));
  }
  else {
    if( ga<fhmx ) {
      aas = 1+fhmn/fhmx;
      at = (fhmx-fhmn)/fhmx;
      au = ga*ga/(fhmx*fhmx);
      c = 2/(sqrt(aas*aas+au)+sqrt(at*at+au));
      ssmin = fhmn*c;
      ssmax = fhmx/c;
    }
    else {
      au = fhmx/ga;
      if( au==0 ) {
        // Avoid possible harmful underflow if exponent range
        // asymmetric (true SSMIN may not underflow even if
        // AU underflows)
        ssmin = fhmn*fhmx/ga;
        ssmax = ga;
      }
      else {
        aas = 1+fhmn/fhmx;
        at = (fhmx-fhmn)/fhmx;
        c = 1/(sqrt(1+aas*aas*au*au)+sqrt(1+at*at*au*au));
        ssmin = fhmn*c*au;
        ssmin = ssmin+ssmin;
        ssmax = ga/(c+c);
      }
    }
  }
}


/*************************************************************************
SvdV2X2 computes the singular value decomposition of a 2-by-2
triangular matrix
   [  F   G  ]
   [  0   H  ].
On return, abs(SSMAX) is the larger singular value, abs(SSMIN) is the
smaller singular value, and (CSL,SNL) and (CSR,SNR) are the left and
right singular vectors for abs(SSMAX), giving the decomposition

   [ CSL  SNL ] [  F   G  ] [ CSR -SNR ]  =  [ SSMAX   0   ]
   [-SNL  CSL ] [  0   H  ] [ SNR  CSR ]     [  0    SSMIN ].
*************************************************************************/
void svdv2x2(double f, double g, double h, double& ssmin,
             double& ssmax, double& snr, double& csr,
             double& snl, double& csl)
{
  double a;
  double clt;
  double crt;
  double d;
  double l;
  double m;
  double mm;
  double r;
  double s;
  double slt;
  double srt;
  double t;
  double temp;
  double tsign;
  double tt;
  double v;

  double ft = f;
  double fa = fabs(ft);
  double ht = h;
  double ha = fabs(h);
  // PMAX points to the maximum absolute element of matrix
  //  PMAX = 1 if F largest in absolute values
  //  PMAX = 2 if G largest in absolute values
  //  PMAX = 3 if H largest in absolute values
  int pmax = 1;
  bool swp = ha>fa;
  if(swp) {
    // Now FA .ge. HA
    pmax = 3;
    temp = ft;
    ft = ht;
    ht = temp;
    temp = fa;
    fa = ha;
    ha = temp;
  }
  double gt = g;
  double ga = fabs(gt);
  if(ga==0) { // Diagonal matrix
    ssmin = ha;
    ssmax = fa;
    clt = 1;
    crt = 1;
    slt = 0;
    srt = 0;
  }
  else {
    bool gasmal = true;
    if(ga>fa) {
      pmax = 2;
      if( fa/ga<1e-15 ) {
        // Case of very large GA
        gasmal = false;
        ssmax = ga;
        if( ha>1 ) {
          v = ga/ha;
          ssmin = fa/v;
        }
        else {
          v = fa/ga;
          ssmin = v*ha;
        }
        clt = 1;
        slt = ht/gt;
        srt = 1;
        crt = ft/gt;
      }
    }
    if( gasmal ) {
      // Normal case
      d = fa-ha;
      if( d==fa )
        l = 1;
      else
        l = d/fa;
      m = gt/ft;
      t = 2-l;
      mm = m*m;
      tt = t*t;
      s = sqrt(tt+mm);
      if( l==0 )
        r = fabs(m);
      else
        r = sqrt(l*l+mm);
      a = 0.5*(s+r);
      ssmin = ha/a;
      ssmax = fa*a;
      if( mm==0 ) {
        // Note that M is very tiny
        if( l==0 )
          t = extsignbdsqr(double(2), ft)*extsignbdsqr(double(1), gt);
        else
          t = gt/extsignbdsqr(d, ft)+m/t;
      }
      else
        t = (m/(s+t)+m/(r+l))*(1+a);
      l = sqrt(t*t+4);
      crt = 2/l;
      srt = t/l;
      clt = (crt+srt*m)/a;
      v = ht/ft;
      slt = v*srt/a;
    }
  }
  if(swp) {
    csl = srt;
    snl = crt;
    csr = slt;
    snr = clt;
  }
  else {
    csl = clt;
    snl = slt;
    csr = crt;
    snr = srt;
  }
  // Correct signs of SSMAX and SSMIN
  if(pmax==1)
    tsign = extsignbdsqr(1.0, csr)*extsignbdsqr(1.0, csl)*extsignbdsqr(1.0, f);
  if(pmax==2)
    tsign = extsignbdsqr(1.0, snr)*extsignbdsqr(1.0, csl)*extsignbdsqr(1.0, g);
  if(pmax==3)
    tsign = extsignbdsqr(1.0, snr)*extsignbdsqr(1.0, snl)*extsignbdsqr(1.0, h);
  ssmax = extsignbdsqr(ssmax, tsign);
  ssmin = extsignbdsqr(ssmin, tsign*extsignbdsqr(1.0, f)*extsignbdsqr(1.0, h));
}



