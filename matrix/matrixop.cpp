#include "matrixop.h"
#include "matrixlu.h"
#include "matrix.h"
#include <vector>
#include <cmath>
#include <functional>
#include <algorithm>

namespace matrix {
void MatrixOp::SolveSystem(const Matrix &m,
                          std::vector<double> & vb)
{
  Matrix loc_m(m);
  if(!Inverse(loc_m))
    return;
  vb = Mult(loc_m, vb);
}

std::vector<double> MatrixOp::Mult(const Matrix &matrix, const std::vector<double> & v)
{
  std::vector<double> temp(matrix.mat().size());
  int j, k;
  for (j = 0; j < temp.size(); ++j) {
    for (temp[j] = 0.0, k = 0; k < matrix.mat()[j].size(); ++k)
      temp[j] += matrix.mat()[j][k] * v[k];
  }
  return temp;
}

bool MatrixOp::Inverse(Matrix& a)
{
  const std::vector<size_t>& pivots = MatrixLU::LU(a);
  return MatrixLU::Inv(a, pivots);
}

std::vector<double> MatrixOp::Mult(const std::vector<double> & v, double a)
{
  std::vector<double> temp(v.begin(), v.end());
  std::for_each(temp.begin(), temp.end(), std::bind2nd(std::multiplies<double>(), a));
  return temp;
}

Matrix MatrixOp::Mult(const Matrix &lhs, const Matrix &rhs)
{
  int ysize = lhs.mat().size();
  int xsize = lhs.mat()[0].size();
  int otherysize = rhs.mat().size();
  int otherxsize = rhs.mat()[0].size();

  std::vector<std::vector<double> > temp(ysize, std::vector<double> (otherxsize));
  int r1 = ysize, c1 = xsize, r2 = otherysize, c2 = otherxsize;
  int i, j, k;
  double tx;
  for (i = 0; i < ysize; ++i) {
    for (j = 0; j < otherxsize; ++j) {
      for (k = 0, tx = 0; k < xsize; ++k)
        tx += lhs.mat()[i][k] * rhs.mat()[k][j];
      temp[i][j] = tx;
    }
  }
  return Matrix(temp);
}

Matrix MatrixOp::Submatrix(const Matrix &matrix, int r1, int c1, int r2p1, int c2p1 )
{
  std::vector<std::vector<double> > tm(r2p1-r1, std::vector<double>(c2p1-c1));
  for (int i=r1 ; i<r2p1 ; ++i)
    std::copy(matrix.mat()[i].begin()+c1, matrix.mat()[i].begin()+c2p1, tm[i].begin());
  return Matrix(tm);
}

Matrix MatrixOp::Transpose( const Matrix &matrix)
{
  if(matrix.mat().empty())
    return matrix;
  int xsize = (int) matrix.mat().front().size();
  int ysize = (int) matrix.mat().size();
  std::vector<std::vector<double> > temp(xsize,std::vector<double>(ysize));
  for (int i=0 ; i<xsize ; ++i)
    for (int j=0 ; j<ysize ; ++j)
      temp[i][j] = matrix.mat()[j][i];
  return Matrix(temp);
}
}//namespace matrix

