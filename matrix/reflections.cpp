﻿#include "reflections.h"
#include <cmath>
#include <numeric>
#include <functional>
#include <algorithm>
#include <limits>

/*************************************************************************
Генерация элементарного преобразования отражения

Подпрограмма генерирует элементарное отражение H порядка N, такое, что для
заданного X выполняется следующее равенство:

    ( X(1) )   ( Beta )
H * (  ..  ) = (  0   )
    ( X(n) )   (  0   )

где
              ( V(1) )
H = 1 - Tau * (  ..  ) * ( V(1), ..., V(n) )
              ( V(n) )

причем первая компонента вектора V равна единице.

Входные параметры:
    X       -   вектор. Массив с нумерацией элементов [1..N]
    N       -   порядок отражения

Выходные параметры:
    X       -   компоненты с 2 по N замещается вектором V. Первая
                компонента замещается параметром Beta.
    Tau     -   скалярная величина Tau. Равно 0 (если вектор X - нулевой),
                в противном случае 1 <= Tau <= 2.

Данная подпрограмма является модификацией подпрограмм DLARFG из библиотеки
LAPACK.

  -- LAPACK auxiliary routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     September 30, 1994
*************************************************************************/
double generatereflection(std::vector<double>& x, size_t n)
{
  if(n<2)
    return 0;
  // XNORM = DNRM2( N-1, X, INCX )
  double alpha = x[0];
  double mx = *std::max_element(x.begin()+1, x.begin()+n);
  double xnorm = std::sqrt(std::inner_product(x.begin()+1, x.begin()+n, x.begin()+1, 0.0));//sqrt(sum_i=1^n x_i^2)
  if(std::fabs(mx)<std::numeric_limits<double>::min())
    return 0;
  if(std::fabs(xnorm)<std::numeric_limits<double>::min())
    return 0;
  // general case
  mx = std::max(std::fabs(alpha), std::fabs(xnorm));
  double beta = -mx*std::sqrt((alpha*alpha+xnorm*xnorm)/(mx*mx));
  if(alpha<0)
    beta = -beta;
  double tau = (beta-alpha)/beta;
  double v = 1/(alpha-beta);
  for(size_t j = 1; j < n; ++j)
    x[j]*=v;
  x[0] = beta;
  return tau;
}


/*************************************************************************
Применение элементарного отражения к прямоугольной матрице размером MxN

Алгоритм умножает слева матрицу на элементарное преобразование  отражения,
заданное    столбцом   V   и   скалярной   величиной   Tau  (см.  описание
GenerateReflection). Преобразованию подвергается не вся матрица, а  только
её часть (строки от M1 до M2, столбцы от N1 до N2). Элементы, не  попавшие
в указанную подматрицу, остаются без изменений.

Входные параметры:
    C   -   матрица,  к  которой  применяется  преобразование.
    Tau -   скаляр, задающий преобразование.
    V   -   столбец, задающий преобразование. Массив с нумерацией элементов
            [1..M2-M1+1]
    M1,M2   -   диапазон строк, затрагиваемых преобразованием
    N1,N2   -   диапазон столбцов, затрагиваемых преобразованием
    WORK    -   рабочий массив с нумерацией элементов от N1 до N2

Выходные параметры:
    C   -   результат умножения входной матрицы C на матрицу преобразования,
            заданного Tau и V. Если N1>N2 или M1>M2, то C не меняется.

  -- LAPACK auxiliary routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     September 30, 1994
*************************************************************************/
void applyreflectionfromtheleft(matrix::Matrix& c, double tau,
                                const std::vector<double>& v,
                                int m1, int m2, int n1, int n2,
                                std::vector<double>& work)
{
//  double t;
//  int i;
  if(tau==0||n1>=n2||m1>=m2 )
    return;
  // w := C' * v
  for(size_t i = n1; i < n2; i++)
    work[i] = 0;
  for(size_t i = m1; i < m2; i++) {
    double t = v[i-m1];
    for(size_t k=n1; k<n2; ++k)
      work[k]+=t*c.elem(i,k); // ap::vadd(&work(n1), &c(i, n1), ap::vlen(n1,n2), t);
  }

  // C := C - tau * v * w'
  for(size_t i = m1; i < m2; i++) {
    double t = v[i-m1]*tau;
    for(size_t k=n1;k<n2; ++k)
      c.elem(i,k)-=t*work[k]; // ap::vsub(&c(i, n1), &work(n1), ap::vlen(n1,n2), t);
  }
}


/*************************************************************************
Применение элементарного отражения к прямоугольной матрице размером MxN

Алгоритм умножает справа матрицу на элементарное преобразование отражения,
заданное    столбцом   V   и   скалярной   величиной   Tau  (см.  описание
GenerateReflection). Преобразованию подвергается не вся матрица, а  только
её часть (строки от M1 до M2, столбцы от N1 до N2). Элементы, не  попавшие
в указанную подматрицу, остаются без изменений.

Входные параметры:
    C   -   матрица,  к  которой  применяется  преобразование.
    Tau -   скаляр, задающий преобразование.
    V   -   столбец, задающий преобразование. Массив с нумерацией элементов
            [1..N2-N1+1]
    M1,M2   -   диапазон строк, затрагиваемых преобразованием
    N1,N2   -   диапазон столбцов, затрагиваемых преобразованием
    WORK    -   рабочий массив с нумерацией элементов от M1 до M2

Выходные параметры:
    C   -   результат умножения входной матрицы C на матрицу преобразования,
            заданного Tau и V. Если N1>N2 или M1>M2, то C не меняется.

  -- LAPACK auxiliary routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     September 30, 1994
*************************************************************************/
void applyreflectionfromtheright(matrix::Matrix& c, double tau,
                                 const std::vector<double>& v,
                                 size_t m1, size_t m2, size_t n1, size_t n2,
                                 std::vector<double>& work)
{
  if(std::fabs(tau)<std::numeric_limits<double>::min() || n1>n2 || m1>m2 )
    return;

  // w := C * v
  for(size_t i = m1; i < m2; i++) {
    double t=0;
    for(size_t k=n1; k<n2; ++k)
      t+=c.elem(i,k)*v[k-n1];
//    t = ap::vdotproduct(&c(i, n1), &v(1), ap::vlen(n1,n2));
    work[i] = t;
  }
  // C := C - w * v'
  for(size_t i = m1; i < m2; i++) {
    double t = work[i]*tau;
    for(size_t k=n1; k<n2; ++k)
      c.elem(i,k)-=t*v[k-n1];
//    ap::vsub(&c(i, n1), &v(1), ap::vlen(n1,n2), t);
  }
}
