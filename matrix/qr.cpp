﻿#include "qr.h"
#include "reflections.h"
#include "matrix.h"
#include <iostream>
#include <algorithm>

namespace matrix {
/*************************************************************************
QR-разложение прямоугольной матрицы размером M x N

Входные параметры:
    A   -   матрица A. Нумерация элементов: [0..M-1, 0..N-1]
    M   -   число строк в матрице A
    N   -   число столбцов в матрице A

Выходные параметры:
    A   -   матрицы Q и R в компактной форме (см. ниже)
    Tau -   массив скалярных множителей, участвующих в формировании
            матрицы Q. Нумерация элементов [0.. Min(M-1,N-1)]

Матрица A представляется, как A = QR, где Q ортогональная матрица размером
M x M, а R  верхнетреугольная (или верхнетрапецоидальная) матрица размером
M x N.

После завершения работы подпрограммы на главной диагонали матрицы A и выше
располагаются элементы матрицы R. В массиве Tau и под  главной  диагональю
матрицы A располагаются элементы, формирующие матрицу Q, следующим способом:

Матрица Q представляется, как произведение элементарных отражений

Q = H(0)*H(2)*...*H(k-1),

где k = min(m,n), а каждое H(i) имеет вид

H(i) = 1 - tau * v * (v^T)

где tau скалярная величина, хранящаяся в Tau[I], а v - вещественный вектор
у которого v(0:i-1)=0, v(i)=1, v(i+1:m-1) хранится в элементах A(i+1:m-1,i).

  -- LAPACK routine (version 3.0) --
     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
     Courant Institute, Argonne National Lab, and Rice University
     February 29, 1992.
*************************************************************************/
void MatrixQr::Qr(Matrix& a, std::vector<double> & tau)
{
  size_t m = a.numRows();
  size_t n = a.numCols();
  if(m<=0||n<=0)
    return;
  size_t minmn = std::min(m, n);
  std::vector<double> work(n);
  std::vector<double> t(m);
  tau.resize(minmn);
  // Test the input arguments
  for(size_t i = 0; i < minmn; ++i) {
    // Generate elementary reflector H(i) to annihilate A(i+1:m,i)
    for(size_t j=0; j<m-i; ++j)
      t[j]=a.elem(j+i,i);
//    ap::vmove(t.getvector(1, m-i), a.getcolumn(i, i, m-1));
    tau[i] = generatereflection(t, m-i);
    for(size_t j=0; j<m-i; ++j)
      a.elem(j+i, i) = t[j];
//    ap::vmove(a.getcolumn(i, i, m-1), t.getvector(1, m-i));
    t[0] = 1;
    if(i<n-1) {
      // Apply H(i) to A(i:m-1,i+1:n-1) from the left
      applyreflectionfromtheleft(a, tau[i], t, i, m, i+1, n, work);
    }
  }
}


/*************************************************************************
Частичная "распаковка" матрицы Q из QR-разложения матрицы A.

Входные параметры:
    A       -   матрицы Q и R в упакованной форме.
                Результат работы RMatrixQR
    M       -   число строк в оригинальной матрице A. M>=0
    N       -   число столбцов в оригинальной матрице A. N>=0
    Tau     -   скалярные множители, формирующие Q.
                Результат работы RMatrixQR.
    QColumns-   требуемое число столбцов матрицы Q. M>=QColumns>=0.

Выходные параметры:
    Q       -   первые QColumns столбцов матрицы Q. Массив с нумерацией
                элементов [0..M-1, 0..QColumns-1]. Если QColumns=0, то массив
                не изменяется.
*************************************************************************/
Matrix MatrixQr::UnpackQ(const Matrix& a, const std::vector<double>& tau,
                         size_t qcolumns)
{
  size_t m = a.numRows();
  size_t n = a.numCols();
  if( m<=0||n<=0||qcolumns<=0 )
    return a;

  // init
  size_t minmn = std::min(m, n);
  size_t k = std::min(minmn, qcolumns);
  matrix::Matrix q(m,qcolumns);
  std::vector<double> v(m);
  std::vector<double> work(qcolumns);
  for(size_t i = 0; i < m; i++) {
    for(size_t j = 0; j < qcolumns; j++) {
      q.elem(i,j)=(double)(i==j);
    }
  }
  // unpack Q
  for(size_t i = k-1; i > 0; i--) {
    // Apply H(i)
    for(size_t j=i; j<m; ++j)
      v[j-i]=a.elem(j,i);
    v[0] = 1;
    applyreflectionfromtheleft(q, tau[i], v, i, m, 0, qcolumns, work);
  }
  return q;
}

/*************************************************************************
Распаковка матрицы R из QR-разложения матрицы A.

Входные параметры:
    A       -   матрицы Q и R в упакованной форме.
                Результат работы подпрограммы RMatrixQR
    M       -   число строк в оригинальной матрице A. M>=0
    N       -   число столбцов в оригинальной матрице A. N>=0

Выходные параметры:
    R       -   матрица R. Массив с нумерацией элементов [0..M-1, 0..N-1].

*************************************************************************/
Matrix MatrixQr::UnpackR(const Matrix& a)
{
  size_t m = a.numRows();
  size_t n = a.numCols();
  if(m<=0||n<=0)
    return a;
  size_t k = std::min(m, n);
  matrix::Matrix r(m,n);
  for(size_t i = 0; i < n; i++)
    r.elem(0,i) = 0;
  for(size_t i = 1; i < m; i++)
    for(size_t j=0; j<n; ++j)
      r.elem(i,j) = r.elem(0,j);
  for(size_t i = 0; i < k; i++)
    for(size_t j=i;j<n; ++j)
      r.elem(i,j) = a.elem(i,j);
  return r;
}

MatrixQr::MatrixQr(Matrix &a) : _a(a)
{
  Qr(_a, _tau);
}

const Matrix &MatrixQr::a() const
{
  return _a;
}

const std::vector<double> MatrixQr::tau() const
{
  return _tau;
}

Matrix MatrixQr::unpackQ(size_t num_col) const
{
  return UnpackQ(_a, _tau, num_col);
}

Matrix MatrixQr::unpackR() const
{
  return UnpackR(_a);
}
} //namespace matrix
