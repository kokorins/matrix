#ifndef MATRIXOP_H_
#define MATRIXOP_H_
#include <vector>
namespace matrix {
struct Matrix;
class MatrixOp {
public:
  static void SolveSystem(const Matrix &matrix, std::vector<double>& vb);
  static bool Inverse(Matrix &a);
  static std::vector<double> Mult(const Matrix& matrix, const std::vector<double>& v);
  static Matrix Mult(const Matrix& lhs, const Matrix& rhs);
  static std::vector<double> Mult(const std::vector<double>& v, double a);
  static Matrix Submatrix(const Matrix& matrix, int r1, int c1, int r2p1, int c2p1);
  static Matrix Transpose(const Matrix&);
};
} //namespace matrix

#endif /* MATRIXOP_H_ */
