#include "matrixlu.h"
#include "matrix.h"
#include <vector>
#include <cmath>
#include <functional>
#include <algorithm>
#include <limits>

namespace matrix {
std::vector<size_t> MatrixLU::LU(Matrix &a)
{
  size_t m = a.numRows();
  size_t n = a.numCols();
  std::vector<size_t> pivots(std::min(m, n));
  if(m==0 || n==0)
    return pivots;
  for(int j = 0; j < std::min(m, n); ++j) {
    // Find pivot and test for singularity.
    int jp = j;
    for(size_t i = j+1; i < m; ++i) {
      if(std::fabs(a.mat()[i][j])>std::fabs(a.mat()[jp][j])) {
        jp = i;
      }
    }
    pivots[j] = jp;
    if(std::fabs(a.mat()[jp][j])>std::numeric_limits<double>::min()) {
      //Apply the interchange to rows
      if(jp!=j) {
        for(size_t i=0; i<a.numRows(); ++i)
          std::swap(a.elem(j,i), a.elem(jp,i));
      }
      //Compute elements J+1:M of J-th column.
      if(j<m) {
        jp = j+1;
        double s = 1/a.mat()[j][j];
        for(size_t i=jp; i<m; ++i)
          a.elem(i,j)*=s;
      }
    }
    if(j<std::min(m, n)-1) {
      //Update trailing submatrix.
      jp = j+1;
      for(size_t i = j+1; i < m; i++) {
        double s = a.mat()[i][j];
        for(size_t k=jp; k<n; ++k)
          a.elem(i,k) -= s*a.elem(j,k);
      }
    }
  }
  return pivots;
}

double MatrixLU::Det(const Matrix &a,
                     const std::vector<size_t> &pivots)
{
  double result = 1;
  int s = 1;
  for(size_t i = 0; i < a.numRows(); i++) {
    result *= a.elem(i,i);
    if(pivots[i]!=i)
      s = -s;
  }
  return result*s;
}

bool MatrixLU::Inv(Matrix& a, const std::vector<size_t>& pivots)
{
  size_t n = a.numCols();
  if( n==0 )
    return true;
  int jp;
  double v;
  std::vector<double> work(n);
  // Form inv(U)
  if(!TrInv(a, true, false))
    return false;
  // Solve the equation inv(A)*L = inv(U) for inv(A).
  for(int j = n-1; j >= 0; j--) {
    // Copy current column of L to WORK and replace with zeros.
    for(int i = j+1; i <= n-1; i++) {
        work[i] = a.elem(i,j);
        a.elem(i,j) = 0;
    }
    // Compute current column of inv(A).
    if(j<n-1) {
      for(int i = 0; i < n; i++) {
        v = 0;
        for(size_t k=j+1; k<n; ++k)
          v+=a.elem(i,k)*work[k];
        a.elem(i,j) -= v;
      }
    }
  }
  // Apply column interchanges.
  for(int j = n-2; j >= 0; j--) {
    jp = pivots[j];
    if( jp!=j ) {
      for(size_t i=0; i<n; ++i)
        std::swap(a.elem(i,j),a.elem(i,jp));
    }
  }
  return true;
}

bool MatrixLU::TrInv(Matrix &a, bool isupper, bool isunittriangular)
{
  double v;
  double ajj;
  size_t n = a.numRows();
  std::vector<double> t(n);
  bool nounit = !isunittriangular; // Test the input parameters
  if(isupper) {
    // Compute inverse of upper triangular matrix.
    for(int j = 0; j<n; j++) {
      if(nounit) {
        if(std::fabs(a.elem(j,j))<std::numeric_limits<double>::min())
          return false;
        a.elem(j,j) = 1/a.elem(j,j);
        ajj = -a.elem(j,j);
      }
      else
        ajj = -1;
      // Compute elements 1:j-1 of j-th column.
      if(j>0) {
        for(size_t k=0; k<j; ++k) {
          t[k] = a.elem(k, j);
        }
        for(int i = 0; i < j; i++) {
          if(i<j-1) {
            v = 0;
            for(size_t k=i+1; k<j; ++k)
              v += a.elem(i,k)*t[k];
          }
          else
            v = 0;
          if(nounit)
            a.elem(i,j) = v+a.elem(i,i)*t[i];
          else
            a.elem(i,j) = v+t[i];
        }
        for(size_t k=0; k<j; ++k)
          a.elem(k,j)*=ajj;
      }
    }
  }
  else {
    // Compute inverse of lower triangular matrix.
    for(int j = n-1; j >= 0; j--) {
      if(nounit) {
        if(std::fabs(a.elem(j,j))<std::numeric_limits<double>::min())
          return false;
        a.elem(j,j) = 1/a.elem(j,j);
        ajj = -a.elem(j,j);
      }
      else
        ajj = -1;
      if(j<n-1) {
        // Compute elements j+1:n of j-th column.
        for(size_t k=j+1; k<n; ++k)
          t[k] = a.elem(k, j);
        for(int i = j+1; i<n; i++) {
          if( i>j+1 ) {
            v = 0;
            for(size_t k=j+1; k<i; ++k)
              v += a.elem(i,k)*t[k];
          }
          else
            v = 0;
          if( nounit )
            a.elem(i,j) = v+a.elem(i,i)*t[i];
          else
            a.elem(i,j) = v+t[i];
        }
        for(size_t k=j+1; k<n; ++k)
          a.elem(k,j)*=ajj;
      }
    }
  }
  return true;
}
}//namespace matrix

