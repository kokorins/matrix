#ifndef MATRIX_H_
#define MATRIX_H_
#include <vector>
#include <cstddef>
#include <ostream>

namespace matrix {
struct Matrix {
public:
  explicit Matrix(const std::vector<std::vector<double> >& mat);
  Matrix(size_t num_col, size_t num_row);
  Matrix(size_t num_col, size_t num_row, double const*const);
  const std::vector<std::vector<double> >& mat()const;
  double& elem(size_t col, size_t row);
  double elem(size_t col, size_t row)const;
  size_t numRows()const;
  size_t numCols()const;
  double det()const;
  Matrix inverse()const;
  Matrix tr()const;
public:
  /**
   * Squered identity matrix
   * \param n size of matrix
   */
  static Matrix Identity(size_t n);
  static Matrix Mult(const Matrix&a, const Matrix&b);
public:
  std::vector<std::vector<double> > matrix;
};

std::ostream& operator<<(std::ostream& out, const Matrix& m);

} //namespace matrix

#endif /* MATRIX_H_ */
