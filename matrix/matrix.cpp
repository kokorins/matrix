#include "matrix.h"
#include "matrixlu.h"
#include "matrixop.h"
#include <vector>
#include <cmath>
#include <functional>
#include <algorithm>

namespace matrix {
Matrix::Matrix(const std::vector<std::vector<double> >& mat):matrix(mat) {}

Matrix::Matrix(size_t num_col, size_t num_row) : matrix(num_row, std::vector<double>(num_col)) {}

Matrix::Matrix(size_t num_col, size_t num_row, const double *const val) :
    matrix(num_row, std::vector<double>(num_col))
{
  for(size_t i=0; i<matrix.size(); ++i)
    for(size_t j=0; j<matrix[i].size(); ++j)
      matrix[i][j] = val[i*num_col+j];
}

const std::vector<std::vector<double> > &Matrix::mat() const
{
  return matrix;
}

double &Matrix::elem(size_t col, size_t row)
{
  return matrix[col][row];
}

double Matrix::elem(size_t col, size_t row) const
{
  return matrix[col][row];
}

size_t Matrix::numRows() const
{
  return matrix.size();
}

size_t Matrix::numCols() const
{
  if(matrix.empty())
    return 0;
  return matrix.front().size();
}

double Matrix::det() const
{
  Matrix a(*this);
  const std::vector<size_t>& pivots = MatrixLU::LU(a);
  return MatrixLU::Det(a, pivots);
}

Matrix Matrix::inverse() const
{
  Matrix a(*this);
  if(MatrixOp::Inverse(a))
    return a;
  return Matrix::Identity(this->numRows());
}

std::ostream& operator<<(std::ostream& out, const Matrix& m)
{
  for(size_t i=0; i<m.mat().size(); ++i) {
    for(size_t j=0; j<m.mat()[i].size(); ++j) {
      out<<m.mat()[i][j]<<" ";
    }
    out<<std::endl;
  }
  return out;
}

Matrix Matrix::Identity(size_t n)
{
  Matrix id(n,n);
  for(size_t i=0; i<n; ++i)
    id.elem(i,i) = 1;
  return id;
}

Matrix Matrix::Mult(const Matrix &a, const Matrix &b)
{
  Matrix res(a.numRows(), b.numCols());
  for(size_t i=0; i<a.numRows(); ++i)
    for(size_t j=0; j<b.numCols(); ++j) {
      double elem = 0;
      for(size_t k=0; k<b.numRows(); ++k)
        elem += a.elem(i,k)*b.elem(k,j);
      res.elem(i,j) = elem;
    }
  return res;
}

Matrix Matrix::tr() const
{
  Matrix res(numRows(), numCols());
  for(size_t i=0; i<numRows(); ++i)
    for(size_t j=0; j<numCols(); ++j)
      res.elem(j,i) = elem(i,j);
  return res;
}
}//namespace matrix

