﻿#include "bidiagonal.h"

#include "reflections.h"
#include "matrix.h"
//#include <iostream>
#include <algorithm>

namespace matrix {
/*************************************************************************
Приведение прямоугольной матрицы к двухдиагональному виду

Алгоритм  приводит   прямоугольную  матрицу  A  к  двухдиагональному  виду
ортогональными преобразованиями P и Q, такими, что A = Q*B*P'

Входные параметры:
    A   -   исходная матрица A.
            Массив с нумерацией элементов [0..M-1, 0..N-1]
    M   -   число строк в матрице
    N   -   число столбцов в матрице

Выходные параметры:
    A   -   матрицы Q, B, P в упакованной форме (см. ниже).
    TauQ-   скалярные множители, участвующие в формировании матрицы Q
    TauP-   скалярные множители, участвующие в формировании матрицы Q

В результате работы алгоритма главная диагональ матрицы A и одна из побочных
замещаются бидиагональной матрицей B, а  в  остальных  элементах  хранятся
элементарные  отражения,  формирующие  матрицы  Q  и  P размером MxM и NxN
соответственно.

Если M>=N, то матрица B - верхняя двухдиагональная размером MxN и хранится
в соответствующих элементах матрицы A. Матрица  Q  представляется  в  виде
произведения элементарных отражений  Q = H(0)*H(1)*...*H(n-1), где  H(i) =
= 1-tau*v*v'. Здесь tau - скалярная величина, хранящая в TauQ[i], а вектор
v имеет следующую  структуру:  v(0:i-1)=0, v(i)=1, v(i+1:m-1)  хранится  в
элементах A(i+1:m-1,i). Матрица P имеет вид P = G(0)*G(1)*...*G(n-2),  где
G(i) = 1-tau*u*u'.  Tau хранится в TauP[i], u(0:i)=0, u(i+1)=1, u(i+2:n-1)
хранится в элементах A(i,i+2:n-1).

Если M<N, то матрица B - нижняя двухдиагональная размером MxN, и  хранится
в  соответствующих  элементах  матрицы  A.  Q = H(0)*H(1)*...*H(m-2),  где
H(i) = 1 - tau*v*v',  tau  хранится в TauQ, v(0:i)=0, v(i+1)=1, v(i+2:m-1)
хранится в A(i+2:m-1,i). P = G(0)*G(1)*...*G(m-1), G(i) = 1-tau*u*u',  tau
хранится в TauP,  u(0:i-1)=0, u(i)=1, u(i+1:n-1) хранится в A(i,i+1:n-1).

ПРИМЕР:

m=6, n=5 (m > n):               m=5, n=6 (m < n):

(  d   e   u1  u1  u1 )         (  d   u1  u1  u1  u1  u1 )
(  v1  d   e   u2  u2 )         (  e   d   u2  u2  u2  u2 )
(  v1  v2  d   e   u3 )         (  v1  e   d   u3  u3  u3 )
(  v1  v2  v3  d   e  )         (  v1  v2  e   d   u4  u4 )
(  v1  v2  v3  v4  d  )         (  v1  v2  v3  e   d   u5 )
(  v1  v2  v3  v4  v5 )

здесь vi и ui обозначает векторы, формирующие H(i) и G(i), а d и e -
диагональные и внедиагональные элементы матрицы B.
*************************************************************************/
void MatrixBd::Bd(Matrix& a, std::vector<double>& tauq, std::vector<double>& taup)
{
  size_t m=a.numRows();
  size_t n=a.numCols();
//  double ltau;
  if(n==0||m==0)
    return;
  size_t maxmn = std::max(m, n);
  std::vector<double> work(maxmn+1);
  std::vector<double> t(maxmn+1);
  if(m>=n) {
    tauq.resize(n);
    taup.resize(n);
  }
  else {
    tauq.resize(m);
    taup.resize(m);
  }
  if(m>=n) {
    // Reduce to upper bidiagonal form
    for(size_t i = 0; i < n; i++) {
      // Generate elementary reflector H(i) to annihilate A(i+1:m-1,i)
      for(size_t j=0; j<m-i; ++j) // ap::vmove(t.getvector(1, m-i), a.getcolumn(i, i, m-1));
        t[j] = a.elem(i+j,i);
      tauq[i] = generatereflection(t, m-i);
      for(size_t j=0; j<m-i; ++j) // ap::vmove(a.getcolumn(i, i, m-1), t.getvector(1, m-i));
        a.elem(i+j,i) = t[j];
      t[0] = 1;
      // Apply H(i) to A(i:m-1,i+1:n-1) from the left
      applyreflectionfromtheleft(a, tauq[i], t, i, m, i+1, n, work);
      if(i<n-1) {
        // Generate elementary reflector G(i) to annihilate
        // A(i,i+2:n-1)
        for(size_t j=1; j<n-i; ++j) // ap::vmove(&t(1), &a(i, i+1), ap::vlen(1,n-i-1));
          t[j-1]=a.elem(i,i+j);
        taup[i] = generatereflection(t, n-1-i);
        for(size_t j=1; j<n-i; ++j)
          a.elem(i,i+j) = t[j-1];
//        ap::vmove(&a(i, i+1), &t(1), ap::vlen(i+1,n-1));
        t[0] = 1;
        // Apply G(i) to A(i+1:m-1,i+1:n-1) from the right
        applyreflectionfromtheright(a, taup[i], t, i+1, m, i+1, n, work);
      }
      else
        taup[i] = 0;
    }
  }
  else {
    // Reduce to lower bidiagonal form
    for(size_t i = 0; i < m; i++) {
      // Generate elementary reflector G(i) to annihilate A(i,i+1:n-1)
      for(size_t j=0; j<n-i; ++j)
        t[j] = a.elem(i,i+j);
//      ap::vmove(&t(1), &a(i, i), ap::vlen(1,n-i));
      taup[i] = generatereflection(t, n-i);
      for(size_t j=0; j<n-i; ++j)
        a.elem(i,i+j) = t[j];
//      ap::vmove(&a(i, i), &t(1), ap::vlen(i,n-1));
      t[0] = 1;
      // Apply G(i) to A(i+1:m-1,i:n-1) from the right
      applyreflectionfromtheright(a, taup[i], t, i+1, m, i, n, work);
      if(i<m-1) {
        // Generate elementary reflector H(i) to annihilate
        // A(i+2:m-1,i)
        for(size_t j=0; j<m-i-1; ++j)
          t[j] = a.elem(i+1+j,i);
//        ap::vmove(t.getvector(1, m-1-i), a.getcolumn(i, i+1, m-1));
        tauq[i] = generatereflection(t, m-1-i);
        for(size_t j=0; j<m-i; ++j)
          a.elem(i+j,i) = t[j-1];
//        ap::vmove(a.getcolumn(i, i+1, m-1), t.getvector(1, m-1-i));
        t[0] = 1;
        // Apply H(i) to A(i+1:m-1,i+1:n-1) from the left
        applyreflectionfromtheleft(a, tauq[i], t, i+1, m, i+1, n, work);
      }
      else
        tauq[i] = 0;
    }
  }
}


/*************************************************************************
Частичная "распаковка" матрицы Q, приводящей матрицу  A к двухдиагональной
форме.

Входные параметры:
    QP      -   матрицы Q и P в упакованной форме.
                Результат работы подпрограммы RMatrixBD.
    M       -   число строк в матрице A
    N       -   число столбцов в матрице A
    TAUQ    -   скалярные множители, формирующие матрицу Q.
                Результат работы подпрограммы RMatrixBD.
    QColumns-   требуемое число столбцов матрицы Q. M >= QColumns >= 0

Выходные параметры:
    Q       -   QColumns первых столбцов матрицы Q.
                Массив с нумерацией элементов [0..M-1, 0..QColumns-1]
                Если QColumns=0, то массив не изменяется.

*************************************************************************/
Matrix MatrixBd::UnpackQ(const Matrix& qp, const std::vector<double>& tauq,
                         size_t qcolumns)
{
  size_t m = qp.numRows();
  size_t n = qp.numCols();
  if( m==0||n==0||qcolumns==0 )
    return qp;
  matrix::Matrix q(m, qcolumns);
  for(size_t i = 0; i < m; i++) {
    for(size_t j = 0; j < qcolumns; j++) {
      q.elem(i,j) = (i==j);
    }
  }
  // Calculate
  MultiplyByQ(qp, tauq, q, m, qcolumns, false, false);
  return q;
}

/*************************************************************************
Умножение на матрицу Q, приводящую матрицу A к двухдиагональной форме.

Алгоритм позволяет осуществить по выбору умножение справа или слева, на
матрицу Q или на матрицу Q'.

Входные параметры:
    QP          -   матрицы Q и P в упакованной форме.
                    Результат работы подпрограммы RMatrixBD.
    TAUQ        -   скалярные множители, формирующие матрицу Q.
                    Результат работы подпрограммы RMatrixBD.
    Z           -   умножаемая матрица
                    массив с нумерацией элементов [0..ZRows-1,0..ZColumns-1]
    ZRows       -   число строк в матрице Z. Если FromTheRight=False, то
                    ZRows=M, иначе может быть любым.
    ZColumns    -   число столбцов в матрице Z. Если FromTheRight=True, то
                    ZColumns=M, иначе может быть любым.
    FromTheRight-   умножение справа или слева
    DoTranspose -   умножение на Q или на Q'

Выходные параметры:
    Z       -   Произведение Z и Q
                Массив с нумерацией элементов [0..ZRows-1,0..ZColumns-1]
                Если ZRows=0 или ZColumns=0, то массив не изменяется.

*************************************************************************/
void MatrixBd::MultiplyByQ(const Matrix& qp, const std::vector<double>& tauq,
                           Matrix& z, size_t zrows, size_t zcolumns,
                           bool fromtheright, bool dotranspose)
{
  int i;
  int i1;
  int i2;
  int istep;
  size_t m=qp.numRows();
  size_t n=qp.numCols();
  if(m==0||n==0||zrows==0||zcolumns==0)
    return;
  // init
  size_t mx = std::max(m, n);
  mx = std::max(mx, zrows);
  mx = std::max(mx, zcolumns);
  std::vector<double> v(mx+1);
  std::vector<double> work(mx+1);
  if( m>=n ) {
    // setup
    if( fromtheright ) {
      i1 = 0;
      i2 = n-1;
      istep = +1;
    }
    else {
      i1 = n-1;
      i2 = 0;
      istep = -1;
    }
    if( dotranspose ) {
      std::swap(i1,i2);
      istep = -istep;
    }
    // Process
    i = i1;
    do {
      for(size_t j=0; j<m-i; ++j) // ap::vmove(v.getvector(1, m-i), qp.getcolumn(i, i, m-1));
        v[j] = qp.elem(i+j, i);
      v[0] = 1;
      if(fromtheright)
        applyreflectionfromtheright(z, tauq[i], v, 0, zrows, i, m, work);
      else
        applyreflectionfromtheleft(z, tauq[i], v, i, m, 0, zcolumns, work);
      i += istep;
    }
    while(i!=i2+istep);
  }
  else {
    // setup
    if(fromtheright) {
      i1 = 0;
      i2 = m-2;
      istep = +1;
    }
    else {
      i1 = m-2;
      i2 = 0;
      istep = -1;
    }
    if( dotranspose ) {
      std::swap(i1,i2);
//      i = i1;
//      i1 = i2;
//      i2 = i;
      istep = -istep;
    }
    // Process
    if(m-1>0) {
      i = i1;
      do {
        for(size_t j=0; j<m-i-1; ++j) // ap::vmove(v.getvector(1, m-i-1), qp.getcolumn(i, i+1, m-1));
          v[j] = qp.elem(i+j, i+1);
        v[0] = 1;
        if(fromtheright)
          applyreflectionfromtheright(z, tauq[i], v, 0, zrows, i+1, m, work);
        else
          applyreflectionfromtheleft(z, tauq[i], v, i+1, m, 0, zcolumns, work);
        i += istep;
      }
      while(i!=i2+istep);
    }
  }
}


/*************************************************************************
Частичная "распаковка" матрицы P, приводящей матрицу  A к двухдиагональной
форме. Подпрограмма выводит транспонированную матрицу P.

Входные параметры:
    QP      -   матрицы Q и P в упакованной форме.
                Результат работы подпрограммы RMatrixBD.
    M       -   число строк в матрице A
    N       -   число столбцов в матрице A
    TAUP    -   скалярные множители, формирующие матрицу P.
                Результат работы подпрограммы RMatrixBD.
    PTRows  -   требуемое число строк матрицы P^T. N >= PTRows >= 0

Выходные параметры:
    PT      -   PTRows первых столбцов матрицы PT.
                Массив с нумерацией элементов [0..PTRows-1, 0..N-1]
                Если PTRows=0, то массив не изменяется.

*************************************************************************/
Matrix MatrixBd::UnpackPt(const Matrix& qp, const std::vector<double>& taup,
                          size_t ptrows)
{
  // prepare PT
  matrix::Matrix pt(ptrows, qp.numCols());
  for(size_t i = 0; i < ptrows; i++)
    for(size_t j = 0; j < qp.numCols(); j++)
      pt.elem(i,j) = (i==j);
  // Calculate
  MultiplyByP(qp, taup, pt, ptrows, qp.numCols(), true, true);
  return pt;
}

/*************************************************************************
Умножение на матрицу P, приводящую матрицу A к двухдиагональной форме.

Алгоритм позволяет осуществить по выбору умножение справа или слева, на
матрицу P или на матрицу P'

Входные параметры:
    QP          -   матрицы Q и P в упакованной форме.
                    Результат работы подпрограммы RMatrixBD.
    M           -   число строк в матрице A
    N           -   число столбцов в матрице A
    TAUP        -   скалярные множители, формирующие матрицу P.
                    Результат работы подпрограммы RMatrixBD.
    Z           -   умножаемая матрица
                    массив с нумерацией элементов [0..ZRows-1,0..ZColumns-1]
    ZRows       -   число строк в матрице Z. Если FromTheRight=False, то
                    ZRows=N, иначе может быть любым.
    ZColumns    -   число столбцов в матрице Z. Если FromTheRight=True, то
                    ZColumns=N, иначе может быть любым.
    FromTheRight-   умножение справа или слева
    DoTranspose -   умножение на P или на P'

Выходные параметры:
    Z       -   Произведение Z и P
                Массив с нумерацией элементов [0..ZRows-1,0..ZColumns-1]
                Если ZRows=0 или ZColumns=0, то массив не изменяется.

*************************************************************************/
void MatrixBd::MultiplyByP(const Matrix& qp, const std::vector<double>& taup,
                           Matrix& z, size_t zrows, size_t zcolumns,
                           bool fromtheright, bool dotranspose)
{
  int i;
  int i1;
  int i2;
  int istep;
  size_t m = qp.numRows();
  size_t n = qp.numCols();

  if( m<=0||n<=0||zrows<=0||zcolumns<=0 )
    return;
  size_t mx = std::max(m, n);
  mx = std::max(mx, zrows);
  mx = std::max(mx, zcolumns);
  std::vector<double> v(mx+1);
  std::vector<double> work(mx+1);
  if( m>=n ) {
    // setup
    if( fromtheright ) {
      i1 = n-2;
      i2 = 0;
      istep = -1;
    }
    else {
      i1 = 0;
      i2 = n-2;
      istep = 1;
    }
    if(!dotranspose) {
      std::swap(i1,i2);
      istep = -istep;
    }
    // Process
    if( n-1>0 ) {
      i = i1;
      do {
        for(size_t j=1; j<n-i-1; ++j)
          v[j] = qp.elem(i,i+j+1);
//        ap::vmove(&v(1), &qp(i, i+1), ap::vlen(1,n-1-i));
        v[0] = 1;
        if( fromtheright )
          applyreflectionfromtheright(z, taup[i], v, 0, zrows, i+1, n, work);
        else
          applyreflectionfromtheleft(z, taup[i], v, i+1, n, 0, zcolumns, work);
        i += istep;
      }
      while(i!=i2+istep);
    }
  }
  else {
    // setup
    if( fromtheright ) {
      i1 = m-1;
      i2 = 0;
      istep = -1;
    }
    else {
      i1 = 0;
      i2 = m-1;
      istep = 1;
    }
    if(!dotranspose) {
      std::swap(i1,i2);
      istep = -istep;
    }
    // Process
    i = i1;
    do {
      for(size_t j=1; j<n-i; ++j)
        v[j] = qp.elem(i,i+j);
//      ap::vmove(&v(1), &qp(i, i), ap::vlen(1,n-i));
      v[0] = 1;
      if( fromtheright )
        applyreflectionfromtheright(z, taup[i], v, 0, zrows, i, n, work);
      else
        applyreflectionfromtheleft(z, taup[i], v, i, n, 0, zcolumns, work);
      i += istep;
    }
    while(i!=i2+istep);
  }
}

/*************************************************************************
"Распаковка" главной и побочной диагоналей двухдиагонального разложения
матрицы A.

Входные параметры:
    B       -   Результат работы подпрограммы RMatrixBD.
    M       -   число строк в матрице B
    N       -   число столбцов в матрице B

Выходные параметры:
    IsUpper -   если матрица верхняя двухдиагональная, то True,
                иначе - False.
    D       -   главная диагональ.
                Массив с нумерацией элементов [0..Min(M,N)-1]
    E       -   побочная диагональ (верхняя или нижняя, в зависимости от
                параметра IsUpper).
                Массив с нумерацией элементов [0..Min(M,N)-2],   последний
                элемент не используется.

*************************************************************************/
void MatrixBd::UnpackDiagonals(const Matrix& b, bool& isupper,
                               std::vector<double>& d, std::vector<double>& e)
{
  size_t m = b.numRows();
  size_t n = b.numCols();
  isupper = (m>=n);
  if(m==0||n==0)
    return;
  if(isupper) {
    d.resize(n);
    e.resize(n-1);
    for(size_t i = 0; i < n-1; i++) {
      d[i] = b.elem(i,i);
      e[i] = b.elem(i,i+1);
    }
    d[n-1] = b.elem(n-1,n-1);
  }
  else {
    d.resize(m);
    e.resize(m-1);
    for(size_t i = 0; i < m-1; i++) {
      d[i] = b.elem(i,i);
      e[i] = b.elem(i+1,i);
    }
    d[m-1] = b.elem(m-1,m-1);
  }
}

MatrixBd::MatrixBd(Matrix &a) : _a(a)
{
  Bd(_a, _tauq, _taup);
}

const Matrix &MatrixBd::a() const
{
  return _a;
}

const std::vector<double> &MatrixBd::taup() const
{
  return _taup;
}

const std::vector<double> &MatrixBd::tauq() const
{
  return _tauq;
}

Matrix MatrixBd::unpackQ(size_t num_cols) const
{
  return UnpackQ(_a, _tauq, num_cols);
}

Matrix MatrixBd::unpackPt(size_t num_rows) const
{
  return UnpackPt(_a, _taup, num_rows);
}

bool MatrixBd::unpackDiagonals(std::vector<double> &d,
                               std::vector<double> &e) const
{
  bool is_upper;
  UnpackDiagonals(_a, is_upper, d, e);
  return is_upper;
}

void MatrixBd::multiplyByQ(Matrix &z, size_t zrows, size_t zcolumns, bool fromtheright, bool dotranspose) const
{
  MultiplyByQ(_a, _tauq, z, zrows, zcolumns, fromtheright, dotranspose);
}

void MatrixBd::multiplyByP(Matrix &z, size_t zrows, size_t zcolumns, bool fromtheright, bool dotranspose) const
{
  MultiplyByP(_a, _taup, z, zrows, zcolumns, fromtheright, dotranspose);
}

} //namespace matrix
