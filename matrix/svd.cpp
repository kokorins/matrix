﻿#include "svd.h"
#include "reflections.h"
#include "bidiagonal.h"
#include "qr.h"
#include "lq.h"
//#include "blas.h"
#include "rotations.h"
#include "bdsvd.h"
#include <iostream>
#include <iterator>
#include<algorithm>


/*************************************************************************
Сингулярное разложение прямоугольной матрицы.

Алгоритм вычисляет сингулярное разложение матрицы размером MxN:
A = U * S * V^T

Алгоритм находит сингулярные значения и, опционально,  матрицы  U  и  V^T.
При этом возможно как нахождение первых  min(M,N)  столбцов  матрицы  U  и
строк матрицы V^T (сингулярных векторов), так и  полных  матриц  U  и  V^T
(размером MxM и NxN).

Обратите внимание, что подпрограмма возвращает матрицу V^T, а не V.

Входные параметры:
    A           -   разлагаемая матрица.
                    Массив с нумерацией элементов [0..M-1, 0..N-1]
    M           -   число строк в матрице A
    N           -   число столбцов в матрице A
    UNeeded     -   0, 1 или 2. Подробнее см. описание параметра U
    VTNeeded    -   0, 1 или 2. Подробнее см. описание параметра VT
    AdditionalMemory-
                    если параметр:
                     * равен 0, то алгоритм не использует дополнительную
                       память (меньше требования к ресурсам, ниже скорость).
                     * равен 1, то алгоритм может использовать дополнительную
                       память в размере min(M,N)*min(M,N) вещественных чисел.
                       В ряде случаев увеличивает скорость алгоритма.
                     * равен 2, то алгоритм может использовать дополнительную
                       память в размере M*min(M,N) вещественных чисел. В ряде
                       случаев это позволяет добиваться максимальной скорости.
                    Рекомендуемое значение параметра: 2.

Выходные параметры:
    W       -   содержит сингулярные значения, упорядоченные по убыванию.
    U       -   если UNeeded=0, не изменяется. Левые сингулярные   векторы
                не вычисляются.
                если UNeeded=1, содержит левые сингулярные векторы (первые
                min(M,N) столбцов матрицы U). Массив с нумерацией элементов
                [0..M-1, 0..Min(M,N)-1].
                если UNeeded=2, содержит полную матрицу U. Массив с нумера-
                цией элементов [0..M-1, 0..M-1].
    VT      -   если VTNeeded=0, не изменяется. Правые сингулярные векторы
                не вычисляются.
                если VTNeeded=1,  содержит   правые   сингулярные  векторы
                (первые min(M,N) строк матрицы V^T). Массив  с  нумерацией
                элементов [0..min(M,N)-1, 0..N-1].
                если VTNeeded=2, содержит полную  матрицу  V^T.  Массив  с
                нумерацией элементов [0..N-1, 0..N-1].

*************************************************************************/
bool matrixsvd(matrix::Matrix a,
               int uneeded,
               int vtneeded,
               int additionalmemory,
               std::vector<double>& w,
               matrix::Matrix& u,
               matrix::Matrix& vt)
{
  std::vector<double> e;
  std::vector<double> work;
  size_t m = a.numRows();
  size_t n = a.numCols();

  int i;
  int j;

  if(m==0 || n==0)
    return true;
  bool result = true;

  size_t minmn = std::min(m, n);
  w.resize(minmn);
  int ncu = 0;
  int nru = 0;
  if(uneeded==1) {
    nru = m;
    ncu = minmn;
    u = matrix::Matrix(ncu, nru);
  }
  if(uneeded==2) {
    nru = m;
    ncu = m;
    u = matrix::Matrix(ncu, nru);
  }
  int nrvt = 0;
  int ncvt = 0;
  if(vtneeded==1) {
    nrvt = minmn;
    ncvt = n;
    vt = matrix::Matrix(ncvt, nrvt);
  }
  if(vtneeded==2) {
    nrvt = n;
    ncvt = n;
    vt = matrix::Matrix(ncvt, nrvt);
  }
  // M much larger than N Use bidiagonal reduction with QR-decomposition
  if( m>1.6*n ) {
    if( uneeded==0 ) { // No left singular vectors to be computed
      matrix::MatrixQr qr(a);
//      matrix::MatrixQr::Qr(a, tau);
      for(i = 0; i < n; i++)
        for(j = 0; j < i; j++)
          a.elem(i,j) = 0;
      matrix::MatrixBd bd(a);
//      matrixbd(a, tauq, taup);
      vt = bd.unpackPt(nrvt);//matrixbdunpackpt(a, taup, nrvt);
//      matrixbdunpackdiagonals(a, isupper, w, e);
      bool isupper = bd.unpackDiagonals(w, e);
      matrix::Matrix empty = matrix::Matrix::Identity(0);
      return matrixbdsvd(w, e, isupper, false, empty, empty,vt);
    }
    else { // Left singular vectors (may be full matrix U) to be computed
      matrix::MatrixQr qr(a);
//      matrix::MatrixQr::Qr(a, tau);
      u = qr.unpackQ(ncu);//matrix::MatrixQr::UnpackQ(a, tau, ncu);
      for(i = 0; i < n; i++)
        for(j = 0; j < i; j++)
          a.elem(i,j) = 0;
      matrix::MatrixBd bd(a);
//      matrixbd(a, tauq, taup);
      vt = bd.unpackPt(nrvt);//matrixbdunpackpt(a, taup, nrvt);
      bool isupper = bd.unpackDiagonals(w,e);
//      matrixbdunpackdiagonals(a, isupper, w, e);
      if( additionalmemory<1 ) {
        // No additional memory can be used
        bd.multiplyByQ(u, m, n, true, false);
//        matrixbdmultiplybyq(a, tauq, u, m, n, true, false);
        matrix::Matrix empty = matrix::Matrix::Identity(0);
        result = matrixbdsvd(w, e, isupper, false, u, empty, vt);
      }
      else {
        // Large U. Transforming intermediate matrix T2
        work.resize(std::max(m,n));
//        work.setbounds(1, ap::maxint(m, n));
        matrix::Matrix t2 = bd.unpackQ(n);//matrixbdunpackq(a, tauq, n);
        a = u;
        t2 = t2.tr();
        matrix::Matrix empty = matrix::Matrix::Identity(0);
        result = matrixbdsvd(w, e, isupper, false, empty, t2, vt);
        u = matrix::Matrix::Mult(a, t2.tr());
//        matrixmatrixmultiply(a, 0, m-1, 0, n-1, false, t2, 0, n-1, 0, n-1, true, 1.0, u, 0, m-1, 0, n-1, 0.0, work);
      }
      return result;
    }
  }

  // N much larger than M
  // Use bidiagonal reduction with LQ-decomposition
  if( n>1.6*m ) {
    if( vtneeded==0 ) {
      // No right singular vectors to be computed
      matrix::MatrixQr qr(a);
//      matrix::MatrixQr::Qr(a, tau);
      for(i = 0; i < m; i++)
        for(j = i+1; j < m; j++)
          a.elem(i,j) = 0;
      matrix::MatrixBd bd(a);
//      matrixbd(a, tauq, taup);
      u = bd.unpackQ(ncu);//matrixbdunpackq(a, tauq, ncu);
      bool isupper = bd.unpackDiagonals(w, e);
//      matrixbdunpackdiagonals(a, isupper, w, e);
      work.resize(m);
      u = u.tr();
      matrix::Matrix empty = matrix::Matrix::Identity(0);
      result = matrixbdsvd(w, e, isupper, false, empty, u, empty);
      u = u.tr();
      return result;
    }
    else {
      // Right singular vectors (may be full matrix VT) to be computed
      matrix::MatrixLq lq(a);
//      matrix::MatrixLQ::LQ(a, tau);
      vt = lq.unpackQ(nrvt);
      for(i = 0; i < m; i++)
        for(j = i+1; j < m; j++)
          a.elem(i,j) = 0;
      matrix::MatrixBd bd(a);
//      matrixbd(a, tauq, taup);
      u = bd.unpackQ(ncu);//matrixbdunpackq(a, tauq, ncu);
      bool isupper = bd.unpackDiagonals(w,e);
//      matrixbdunpackdiagonals(a, isupper, w, e);
      work.resize(std::max(m,n));
      u = u.tr();
      if( additionalmemory<1 ) {
        // No additional memory available
        bd.multiplyByP(vt, m, n, false, true);
//        matrixbdmultiplybyp(a, taup, vt, m, n, false, true);
        matrix::Matrix empty = matrix::Matrix::Identity(0);
        result = matrixbdsvd(w, e, isupper, false, empty, u, vt);
      }
      else {
        // Large VT. Transforming intermediate matrix T2
        matrix::Matrix t2 = bd.unpackPt(m);//matrixbdunpackpt(a, taup, m);
        matrix::Matrix empty = matrix::Matrix::Identity(0);
        result = matrixbdsvd(w, e, isupper, false, empty, u, t2);
        a = vt;
        vt = matrix::Matrix::Mult(t2, a);
//        matrixmatrixmultiply(t2, 0, m-1, 0, m-1, false, a, 0, m-1, 0, n-1, false, 1.0, vt, 0, m-1, 0, n-1, 0.0, work);
      }
      u = u.tr();
      return result;
    }
  }
  // M<=N. We can use inplace transposition of U to get rid of columnwise operations
  if( m<=n ) {
//    std::cout<<a;
    //matrixbd(a, tauq, taup);
    matrix::MatrixBd bd(a);
    u = bd.unpackQ(ncu);//matrixbdunpackq(a, tauq, ncu);
    vt = bd.unpackPt(nrvt);//matrixbdunpackpt(a, taup, nrvt);
//    matrixbdunpackdiagonals(a, isupper, w, e);
    bool isupper = bd.unpackDiagonals(w, e);
    work.resize(m);
    u = u.tr();
    matrix::Matrix empty = matrix::Matrix::Identity(0);
    result = matrixbdsvd(w, e, isupper, false, empty, u, vt);
    u = u.tr();
    return result;
  }
  // Simple bidiagonal reduction
//  matrixbd(a, tauq, taup);
  matrix::MatrixBd bd(a);
  u = bd.unpackQ(ncu);//matrixbdunpackq(a, tauq, ncu);
  vt = bd.unpackPt(nrvt);//matrixbdunpackpt(a, taup, nrvt);
  bool isupper = bd.unpackDiagonals(w, e);
//  matrixbdunpackdiagonals(a, isupper, w, e);
  // We cant use additional memory or there is no need in such operations
  if(additionalmemory<2 || uneeded==0) {
    matrix::Matrix empty = matrix::Matrix::Identity(0);
    result = matrixbdsvd(w, e, isupper, false, u, empty, vt);
  }
  else {
    // We can use additional memory
    matrix::Matrix t2 = matrix::Matrix(m, minmn);
    t2 = u.tr();
    matrix::Matrix empty = matrix::Matrix::Identity(0);
    result = matrixbdsvd(w, e, isupper, false, empty, t2, vt);
    u = t2.tr();
  }
  return result;
}



