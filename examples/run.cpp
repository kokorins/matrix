#include <matrix/matrixlu.h>
#include <matrix/matrixop.h>
#include <matrix/matrix.h>
#include <matrix/qr.h>
#include <matrix/reflections.h>
#include <matrix/bidiagonal.h>
#include <matrix/bdsvd.h>
#include <matrix/svd.h>
#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>
#include <iterator>
#include <iostream>

std::ostream& operator<<(std::ostream& out, const std::vector<double>& a)
{
  std::copy(a.begin(), a.end(), std::ostream_iterator<double>(out, " "));
  out<<std::endl;
  return out;
}

void testRandomSvd()
{
  using namespace matrix;
  double aa[]={12,-34,23,4,-3,
               34,-2,23,-4,6,
               34,34,-2,23,-4,
               34,-2,23,-4,2,
               34,-2,21,-4,2};
  Matrix a(5,5, aa);
  std::vector<double> w(5);
  matrix::Matrix u = matrix::Matrix::Identity(5);
  matrix::Matrix vt = matrix::Matrix::Identity(5);
  matrixsvd(a, 2, 2, 2, w, u, vt);
  std::cout<<"singular values: \n"<<w;
  std::cout<<"left:\n"<<u<<"right:\n"<<vt;
}

void testSvd()
{
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
  std::vector<double> w(3);
  matrix::Matrix u = matrix::Matrix::Identity(3);
  matrix::Matrix vt = matrix::Matrix::Identity(3);
  matrixsvd(a, 2, 2, 2, w, u, vt);
  std::cout<<"singular values: \n"<<w;
  std::cout<<"left:\n"<<u<<"right:\n"<<vt;
}

void testBdSvd()
{
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
//  std::vector<double> q,p;
  matrix::MatrixBd bd(a);
//  matrixbd(a, q, p);
  std::vector<double> d,e;
  bool isupper = bd.unpackDiagonals(d, e);
//  matrixbdunpackdiagonals(a, isupper, d, e);
  matrix::Matrix u = matrix::Matrix::Identity(3);
  matrix::Matrix c = matrix::Matrix::Identity(3);
  matrix::Matrix vt = matrix::Matrix::Identity(3);
  matrixbdsvd(d, e, isupper, false, u, c, vt);
  std::cout<<d<<e;
  std::cout<<u<<c<<vt;
}

void testBd()
{
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
//  std::vector<double> q,p;
  matrix::MatrixBd bd(a);
//  matrixbd(a, q, p);
//  std::cout<<a;
//  std::cout<<q;
//  std::cout<<p;
  matrix::Matrix m = bd.unpackQ(a.numCols());//matrixbdunpackq(a, q, a.numCols());
//  std::cout<<m;
  std::vector<double> d,e;
  bool isupper = bd.unpackDiagonals(d, e);
//  matrixbdunpackdiagonals(a, isupper, d, e);
  std::cout<<d<<e;
}

void testQr() {
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
  MatrixQr qr(a);
  Matrix r = qr.unpackR();
  std::cout<<r;
  std::copy(qr.tau().begin(), qr.tau().end(),
            std::ostream_iterator<double>(std::cout, " "));
  std::cout<<std::endl;
}

void testReflection() {
  using namespace boost::assign;
  std::vector<double> x;
  x+=12,-34,23;
  double tau = generatereflection(x, 3);
//  std::cout<<tau<<std::endl;
//  std::copy(x.begin(), x.end(), std::ostream_iterator<double>(std::cout," "));
//  std::cout<<std::endl;
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
  std::vector<double> work(x.size());
//  applyreflectionfromtheleft(a, tau, x, 0, 2, 0, 2, work);
  applyreflectionfromtheright(a, tau, x, 0, 2, 0, 2, work);
  std::cout<<a;
  std::copy(work.begin(), work.end(), std::ostream_iterator<double>(std::cout, " "));
  std::cout<<std::endl;
//  void applyreflectionfromtheright(matrix::Matrix& c,
//                                   double tau,
//                                   const std::vector<double>& v,
//                                   int m1, int m2,
//                                   int n1, int n2,
//                                   std::vector<double>& work);

}

void testLu() {
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
  std::cout<<a;
  const std::vector<size_t>& perm = MatrixLU::LU(a);
  std::cout<<a;
  std::copy(perm.begin(), perm.end(),
            std::ostream_iterator<size_t>(std::cout, " "));
  std::cout<<std::endl;
}

void testTriInv() {
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
  if(!MatrixLU::TrInv(a, false, true))
    std::cout<<"singular\n";
  std::cout<<a;
}

void inv() {
  using namespace matrix;
  double aa[]={12,-34,23,234,34,23,23,-4,34};
  Matrix a(3,3, aa);
  if(!MatrixOp::Inverse(a))
    std::cout<<"singular\n";
  std::cout<<a;
}

int main() {
  testRandomSvd();
//  testSvd();
//  testBdSvd();
  return 0;
}
